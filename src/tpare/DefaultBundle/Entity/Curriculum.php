<?php
namespace tpare\DefaultBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Esta clase contiene los datos del curriculum que va 
 * a dar de alta un usuario registrado
 * 
 *
 *
 * @ORM\Table(name="curriculum")
 * @ORM\Entity(repositoryClass="tpare\DefaultBundle\Entity\CurriculumRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Curriculum {
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\Column(type="datetime")
     */
    protected $fechaActualizacion;//Date
    
     /**
     * @ORM\Column(type="datetime")
     */
    protected $fechaCreacion;//Date
    
    /**
     *
     * @ORM\OneToMany(targetEntity="IdiomaPostulante", mappedBy="curriculum") 
     * 
     */
    protected $idiomas;//IdiomaPostulante
    
    /**
     * @ORM\OneToMany(targetEntity="Conocimiento", mappedBy="curriculum")
     */
    protected $conocimientos;//Conocimiento
    
    
    /**
     * @ORM\OneToOne(targetEntity="DatosPersonales")
     * @ORM\JoinColumn(name="datosPersonales_id", referencedColumnName="id")
     */
    protected $datosPersonales;
    
    /**
     * @ORM\OneToMany(targetEntity="Estudio", mappedBy="curriculum") 
     */
    protected $estudios; //Estudio
    
    /**
     * @ORM\OneToMany(targetEntity="ExperienciaLaboral", mappedBy="curriculum")
     */
    protected $experienciasLaborales;//ExperienciaLaboral

    /**
     * @ORM\OneToMany(targetEntity="Curso", mappedBy="curriculum")
     */
    protected $cursos;
    
    /**
     * @ORM\OneToMany(targetEntity="InformacionExtra", mappedBy="curriculum")
     */
    protected $informacionExtra;
    
    protected $porcentajeReqCumplidos;
    
    protected $puntaje;
    
    private $cumpleReq;
            
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idiomas = new \Doctrine\Common\Collections\ArrayCollection();
        $this->cursos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->conocimientos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->estudios = new \Doctrine\Common\Collections\ArrayCollection();
        $this->experienciasLaborales = new \Doctrine\Common\Collections\ArrayCollection();
        $this->puntaje = 0;
        $this->cumpleRequerimientos = false;
    }
    
    
    public function addPuntaje($puntaje){
        $this->puntaje += $puntaje;
    }
    
    public function getPuntaje(){
        return $this->puntaje;
    }
    
    public function setPorcentajeReqCumplidos($value){
        $this->porcentajeReqCumplidos = $value;
    }
    
    public function getPorcentajeReqCumplidos(){
        return $this->porcentajeReqCumplidos;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fechaActualizacion
     *
     * @param \DateTime $fechaActualizacion
     * @return Curriculum
     * 
     * @ORM\PreUpdate
     */
    public function setFechaActualizacionPreUpdate()
    {
        $this->fechaActualizacion = new \DateTime();
    
        return $this;
    }
    
    /**
     * Set fechaActualizacion
     *
     * @param \DateTime $fechaActualizacion
     * @return Curriculum
     * 
     * @ORM\PrePersist
     */
    public function setFechaActualizacionPrePersist()
    {
        $this->fechaActualizacion = new \DateTime();
    
        return $this;
    }
    
    /**
     * Set fechaCreacion
     *
     * @param \DateTime $fechaActualizacion
     * @return Curriculum
     * 
     * @ORM\PrePersist
     */
    public function setFechaCreacion()
    {
        $this->fechaCreacion = new \DateTime();
    
        return $this;
    }
    
    public function setCumpleReq($boolean){
        $this->cumpleReq = $boolean;
    }
    
    public function getCumpleReq(){
        return $this->cumpleReq;
    }
    /**
     * Get fechaActualizacion
     *
     * @return \DateTime 
     */
    public function getFechaActualizacion()
    {
        return $this->fechaActualizacion;
    }

    /**
     * Set datosPersonales
     *
     * @param \tpare\DefaultBundle\Entity\datosPersonales $datosPersonales
     * @return Curriculum
     */
    public function setDatosPersonales(\tpare\DefaultBundle\Entity\datosPersonales $datosPersonales = null)
    {
        $this->datosPersonales = $datosPersonales;
    
        return $this;
    }

    /**
     * Get datosPersonales
     *
     * @return \tpare\DefaultBundle\Entity\datosPersonales 
     */
    public function getDatosPersonales()
    {
        return $this->datosPersonales;
    }

    /**
     * Add idiomas
     *
     * @param \tpare\DefaultBundle\Entity\idiomaPostulante $idiomas
     * @return Curriculum
     */
    public function addIdioma(\tpare\DefaultBundle\Entity\idiomaPostulante $idiomas)
    {
        $this->idiomas[] = $idiomas;
    
        return $this;
    }

    /**
     * Remove idiomas
     *
     * @param \tpare\DefaultBundle\Entity\idiomaPostulante $idiomas
     */
    public function removeIdioma(\tpare\DefaultBundle\Entity\idiomaPostulante $idiomas)
    {
        $this->idiomas->removeElement($idiomas);
    }

    /**
     * Get idiomas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getIdiomas()
    {
        return $this->idiomas;
    }

    /**
     * Add conocimientos
     *
     * @param \tpare\DefaultBundle\Entity\conocimiento $conocimientos
     * @return Curriculum
     */
    public function addConocimiento(\tpare\DefaultBundle\Entity\conocimiento $conocimientos)
    {
        $this->conocimientos->add($conocimientos);
    
        return $this;
    }

    /**
     * Remove conocimientos
     *
     * @param \tpare\DefaultBundle\Entity\conocimiento $conocimientos
     */
    public function removeConocimiento(\tpare\DefaultBundle\Entity\conocimiento $conocimientos)
    {
        $this->conocimientos->removeElement($conocimientos);
    }

    /**
     * Get conocimientos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getConocimientos()
    {
        return $this->conocimientos;
    }

    /**
     * Add estudios
     *
     * @param \tpare\DefaultBundle\Entity\estudio $estudios
     * @return Curriculum
     */
    public function addEstudio(\tpare\DefaultBundle\Entity\estudio $estudios)
    {
        $this->estudios[] = $estudios;
    
        return $this;
    }

    /**
     * Remove estudios
     *
     * @param \tpare\DefaultBundle\Entity\estudio $estudios
     */
    public function removeEstudio(\tpare\DefaultBundle\Entity\estudio $estudios)
    {
        $this->estudios->removeElement($estudios);
    }

    /**
     * Get estudios
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEstudios()
    {
        return $this->estudios;
    }

    /**
     * Add experienciasLaborales
     *
     * @param \tpare\DefaultBundle\Entity\experienciaLaboral $experienciasLaborales
     * @return Curriculum
     */
    public function addExperienciasLaborale(\tpare\DefaultBundle\Entity\experienciaLaboral $experienciasLaborales)
    {
        $this->experienciasLaborales[] = $experienciasLaborales;
    
        return $this;
    }

    /**
     * Remove experienciasLaborales
     *
     * @param \tpare\DefaultBundle\Entity\experienciaLaboral $experienciasLaborales
     */
    public function removeExperienciasLaborale(\tpare\DefaultBundle\Entity\experienciaLaboral $experienciasLaborales)
    {
        $this->experienciasLaborales->removeElement($experienciasLaborales);
    }

    /**
     * Get experienciasLaborales
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getExperienciasLaborales()
    {
        return $this->experienciasLaborales;
    }


    /**
     * Get fechaCreacion
     *
     * @return \DateTime 
     */
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * Set fechaActualizacion
     *
     * @param \DateTime $fechaActualizacion
     * @return Curriculum
     */
    public function setFechaActualizacion($fechaActualizacion)
    {
        $this->fechaActualizacion = $fechaActualizacion;
    
        return $this;
    }

    /**
     * Add cursos
     *
     * @param \tpare\DefaultBundle\Entity\Curso $cursos
     * @return Curriculum
     */
    public function addCurso(\tpare\DefaultBundle\Entity\Curso $cursos)
    {
        $this->cursos[] = $cursos;
    
        return $this;
    }

    /**
     * Remove cursos
     *
     * @param \tpare\DefaultBundle\Entity\Curso $cursos
     */
    public function removeCurso(\tpare\DefaultBundle\Entity\Curso $cursos)
    {
        $this->cursos->removeElement($cursos);
    }

    /**
     * Get cursos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCursos()
    {
        return $this->cursos;
    }

    /**
     * Add informacioExtra
     *
     * @param \tpare\DefaultBundle\Entity\InformacionExtra $informacioExtra
     * @return Curriculum
     */
    public function addInformacioExtra(\tpare\DefaultBundle\Entity\InformacionExtra $informacioExtra)
    {
        $this->informacioExtra[] = $informacioExtra;
    
        return $this;
    }

    /**
     * Remove informacioExtra
     *
     * @param \tpare\DefaultBundle\Entity\InformacionExtra $informacioExtra
     */
    public function removeInformacioExtra(\tpare\DefaultBundle\Entity\InformacionExtra $informacioExtra)
    {
        $this->informacioExtra->removeElement($informacioExtra);
    }

    /**
     * Get informacioExtra
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getInformacioExtra()
    {
        return $this->informacioExtra;
    }

    /**
     * Add informacionExtra
     *
     * @param \tpare\DefaultBundle\Entity\InformacionExtra $informacionExtra
     * @return Curriculum
     */
    public function addInformacionExtra(\tpare\DefaultBundle\Entity\InformacionExtra $informacionExtra)
    {
        $this->informacionExtra[] = $informacionExtra;
    
        return $this;
    }

    /**
     * Remove informacionExtra
     *
     * @param \tpare\DefaultBundle\Entity\InformacionExtra $informacionExtra
     */
    public function removeInformacionExtra(\tpare\DefaultBundle\Entity\InformacionExtra $informacionExtra)
    {
        $this->informacionExtra->removeElement($informacionExtra);
    }

    /**
     * Get informacionExtra
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getInformacionExtra()
    {
        return $this->informacionExtra;
    }
}