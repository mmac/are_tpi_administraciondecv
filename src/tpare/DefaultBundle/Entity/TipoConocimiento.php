<?php

namespace tpare\DefaultBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *
 * @ORM\Table(name="tipoConocimiento")
 * @ORM\Entity
 * 
 */
class TipoConocimiento {
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     *@ORM\OneToMany(targetEntity="conocimiento", mappedBy="id")
     *
     */
    protected $conocimiento;
    
    /**
     *@ORM\Column(type="string")
     */
    protected $tipoConocimiento;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->conocimiento = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tipoConocimiento
     *
     * @param string $tipoConocimiento
     * @return TipoConocimiento
     */
    public function setTipoConocimiento($tipoConocimiento)
    {
        $this->tipoConocimiento = $tipoConocimiento;
    
        return $this;
    }

    /**
     * Get tipoConocimiento
     *
     * @return string 
     */
    public function getTipoConocimiento()
    {
        return $this->tipoConocimiento;
    }

    /**
     * Add conocimiento
     *
     * @param \tpare\DefaultBundle\Entity\conocimiento $conocimiento
     * @return TipoConocimiento
     */
    public function addConocimiento(\tpare\DefaultBundle\Entity\conocimiento $conocimiento)
    {
        $this->conocimiento[] = $conocimiento;
    
        return $this;
    }

    /**
     * Remove conocimiento
     *
     * @param \tpare\DefaultBundle\Entity\conocimiento $conocimiento
     */
    public function removeConocimiento(\tpare\DefaultBundle\Entity\conocimiento $conocimiento)
    {
        $this->conocimiento->removeElement($conocimiento);
    }

    /**
     * Get conocimiento
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getConocimiento()
    {
        return $this->conocimiento;
    }
}