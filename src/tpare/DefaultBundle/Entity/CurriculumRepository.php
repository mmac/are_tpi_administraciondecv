<?php
namespace tpare\DefaultBundle\Entity;

use Doctrine\ORM\EntityRepository;


/**
 * Description of CurriculumType
 *
 * @author ale
 */
class CurriculumRepository extends EntityRepository {
    
     public function obtenerCVPorOferta($idOferta){
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $qb->select('c')   
           ->from('DefaultBundle:Curriculum', 'c')
           ->innerJoin('DefaultBundle:User', 'u', 'WITH', 'u.curriculum = u.id')    
           ->innerJoin('DefaultBundle:Aplicacion', 'a', 'WITH', 'a.user = u.id'); 
        
        $qb ->andWhere($qb->expr()->eq('a.ofertaLaboral', $idOferta));
        
        return $qb->getQuery()->getResult();
     }
}

?>
