<?php

namespace tpare\DefaultBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @author ale
 * 
 * @ORM\Entity
 * @ORM\Table(name="provincias")
 */
class Provincia {
    /**
     *@ORM\Id
     *@ORM\Column(type="integer")
     *@ORM\GeneratedValue(strategy="AUTO")  
     */
    protected $id;
    
    /**
     *@ORM\Column(type="string", length=100)
     */
    protected $nombre;
    
     /**
     * @ORM\OneToMany(targetEntity="Localidad", mappedBy="provincia")
     **/
    protected $localidades;
    
    public function __construct() {
        $this->localidades = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Provincia
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }


    /**
     * Add localidades
     *
     * @param \tpare\DefaultBundle\Entity\Provincia $localidades
     * @return Provincia
     */
    public function addLocalidade(\tpare\DefaultBundle\Entity\Localidad $localidades)
    {
        $this->localidades[] = $localidades;

        return $this;
    }

    /**
     * Remove localidades
     *
     * @param \tpare\DefaultBundle\Entity\Provincia $localidades
     */
    public function removeLocalidade(\tpare\DefaultBundle\Entity\Provincia $localidades)
    {
        $this->localidades->removeElement($localidades);
    }

    /**
     * Get localidades
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLocalidades()
    {
        return $this->localidades;
    }
}