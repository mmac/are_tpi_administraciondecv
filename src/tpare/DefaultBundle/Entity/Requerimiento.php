<?php

namespace tpare\DefaultBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Un requerimiento que va a tener un puesto de trabajo, el cual va a ser usado para 
 * posteriormente puntuar cada CV.
 *
 * @author ale
 * 
 * @ORM\Entity
 * @ORM\Table(name="requerimientos")
 */
class Requerimiento {
    
    /**
    *@ORM\Id
    *@ORM\Column(type="integer")
    *@ORM\GeneratedValue(strategy="AUTO") 
    */
    protected $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Tecnologia")
     * @ORM\JoinColumn(name="tecnologia_id", referencedColumnName="id")
     * @Assert\NotBlank(message = "Debe seleccionar una tecnología.")
     **/
    protected $tecnologia;
    
    /**
     *@ORM\Column(type="integer")
     *@Assert\NotBlank(message = "Debe ingresar un puntaje.")
     *@Assert\Type(type="integer")
     *@Assert\Range(
     *      min = 1,
     *      max = 10,
     *      minMessage = "El puntaje no puede ser menor a 1.",
     *      maxMessage = "El puntaje no puede ser mayor a 10."
     * )   
     */
    protected $puntaje;
    
    /**
     * @ORM\Column(type="string", length=50)
     * 
     * Validation annotations...
     * @Assert\Choice(
     *  callback = "getNivelTypesCodes",
     *  message = "El tipo de nivel seleccionado no es válido."
     * )
     * @Assert\NotBlank(message = "Debe seleccionar un nivel")
     */
    protected $nivel;
    
    /**
     * @ORM\ManyToOne(targetEntity="OfertaLaboral", inversedBy="requerimientos")
     * @ORM\JoinColumn(name="oferta_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $oferta;
    
    public static function getNivelTypes()
    {
        return array (
          'BAJO'    => "Bajo",
          'MEDIO' => "Medio",
          'AVANZADO' => "Avanzado",
          'EXPERTO' => "Experto",
        );
    }
    
    public static function getNivelTypesCodes()
    {
        return array_keys(self::getNivelTypes());
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set puntaje
     *
     * @param integer $puntaje
     * @return Requerimiento
     */
    public function setPuntaje($puntaje)
    {
        $this->puntaje = $puntaje;
    
        return $this;
    }

    /**
     * Get puntaje
     *
     * @return integer 
     */
    public function getPuntaje()
    {
        return $this->puntaje;
    }

    /**
     * Set nivel
     *
     * @param string $nivel
     * @return Requerimiento
     */
    public function setNivel($nivel)
    {
        $this->nivel = $nivel;
    
        return $this;
    }

    /**
     * Get nivel
     *
     * @return string 
     */
    public function getNivel()
    {
        return $this->nivel;
    }

    /**
     * Set teconologia
     *
     * @param \tpare\DefaultBundle\Entity\Tecnologia $teconologia
     * @return Requerimiento
     */
    public function setTeconologia(\tpare\DefaultBundle\Entity\Tecnologia $teconologia = null)
    {
        $this->teconologia = $teconologia;
    
        return $this;
    }

    /**
     * Get teconologia
     *
     * @return \tpare\DefaultBundle\Entity\Tecnologia 
     */
    public function getTeconologia()
    {
        return $this->teconologia;
    }

    /**
     * Set tecnologia
     *
     * @param \tpare\DefaultBundle\Entity\Tecnologia $tecnologia
     * @return Requerimiento
     */
    public function setTecnologia(\tpare\DefaultBundle\Entity\Tecnologia $tecnologia = null)
    {
        $this->tecnologia = $tecnologia;
    
        return $this;
    }

    /**
     * Get tecnologia
     *
     * @return \tpare\DefaultBundle\Entity\Tecnologia 
     */
    public function getTecnologia()
    {
        return $this->tecnologia;
    }

    /**
     * Set oferta
     *
     * @param \tpare\DefaultBundle\Entity\OfertaLaboral $oferta
     * @return Requerimiento
     */
    public function setOferta(\tpare\DefaultBundle\Entity\OfertaLaboral $oferta = null)
    {
        $this->oferta = $oferta;
    
        return $this;
    }

    /**
     * Get oferta
     *
     * @return \tpare\DefaultBundle\Entity\OfertaLaboral 
     */
    public function getOferta()
    {
        return $this->oferta;
    }
}