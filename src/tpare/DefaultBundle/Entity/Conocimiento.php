<?php
namespace tpare\DefaultBundle\Entity;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 *
 * @ORM\Table(name="conocimiento")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class Conocimiento {
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     *@ORM\ManyToOne(targetEntity="Curriculum", inversedBy="conocimientos")
     *@ORM\JoinColumn(name="curriculum_id", referencedColumnName="id")
     */
    protected $curriculum;
    
   
    
    /**
     *@ORM\Column(type="string", length=30)
     * @Assert\NotBlank(message = "Debe seleccionar un estado del estudio")
     * 
     * Validation annotations...
     * @Assert\Choice(
     *  callback = "getNivelesTypeCodes",
     *  message = "El nivel seleccionado no es válido."
     * )
     */
    protected $nivelConocimiento;
    
    
    public static function getNivelesType()
    {
        return array (
          'BAJO'    => "Bajo",
          'MEDIO' => "Medio",
          'ALTO'=>"Alto",
          'EXPERTO'=>"Experto"
        );
    }
    
    public static function getNivelesTypeCodes()
    {
        return array_keys(self::getNivelesType());
    }
    
      
    /**
     *@ORM\Column(type="string", length=300, nullable=true)
     */
    protected $descripcion;
   
    /**
     *@ORM\ManyToOne(targetEntity="Tecnologia", inversedBy="conocimientos")
     *@ORM\JoinColumn(name="tecnologia_id", referencedColumnName="id")
     *@Assert\NotNull(message="Debe seleccionar una tecnología.")
     */
    protected $tecnologia;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set curriculum
     *
     * @param \tpare\DefaultBundle\Entity\curriculum $curriculum
     * @return conocimiento
     */
    public function setCurriculum(\tpare\DefaultBundle\Entity\curriculum $curriculum = null)
    {
        $this->curriculum = $curriculum;
    
        return $this;
    }

    /**
     * Get curriculum
     *
     * @return \tpare\DefaultBundle\Entity\curriculum 
     */
    public function getCurriculum()
    {
        return $this->curriculum;
    }

    /**
     * Set nivelConocimiento
     *
     * @param string $nivelConocimiento
     * @return Conocimiento
     */
    public function setNivelConocimiento($nivelConocimiento)
    {
        $this->nivelConocimiento = $nivelConocimiento;
    
        return $this;
    }

    /**
     * Get nivelConocimiento
     *
     * @return string 
     */
    public function getNivelConocimiento()
    {
        return $this->nivelConocimiento;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Conocimiento
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    
        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set tipoConocimiento
     *
     * @param \tpare\DefaultBundle\Entity\tipoConocimiento $tipoConocimiento
     * @return Conocimiento
     */
    public function setTipoConocimiento(\tpare\DefaultBundle\Entity\tipoConocimiento $tipoConocimiento = null)
    {
        $this->tipoConocimiento = $tipoConocimiento;
    
        return $this;
    }

    /**
     * Get tipoConocimiento
     *
     * @return \tpare\DefaultBundle\Entity\tipoConocimiento 
     */
    public function getTipoConocimiento()
    {
        return $this->tipoConocimiento;
    }

    /**
     * Set tegnologia
     *
     * @param \tpare\DefaultBundle\Entity\tecnologia $tegnologia
     * @return Conocimiento
     */
    public function setTegnologia(\tpare\DefaultBundle\Entity\tecnologia $tegnologia = null)
    {
        $this->tegnologia = $tegnologia;
    
        return $this;
    }

    /**
     * Get tegnologia
     *
     * @return \tpare\DefaultBundle\Entity\tecnologia 
     */
    public function getTegnologia()
    {
        return $this->tegnologia;
    }

    /**
     * Set tecnologia
     *
     * @param \tpare\DefaultBundle\Entity\tecnologia $tecnologia
     * @return Conocimiento
     */
    public function setTecnologia(\tpare\DefaultBundle\Entity\tecnologia $tecnologia = null)
    {
        $this->tecnologia = $tecnologia;
    
        return $this;
    }

    /**
     * Get tecnologia
     *
     * @return \tpare\DefaultBundle\Entity\tecnologia 
     */
    public function getTecnologia()
    {
        return $this->tecnologia;
    }
}