<?php
namespace tpare\DefaultBundle\Entity;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
/**
 *
 * @ORM\Table(name="idioma")
 * @ORM\Entity
 * 
 */
class Idioma {
    
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\Column(type="string", length=15)
     */
    private $idioma;  
    
    /**
     *@ORM\OneToMany(targetEntity="Curriculum", mappedBy="id")
    */
    protected $curriculum;

    /**
     * @ORM\OneToMany(targetEntity="IdiomaPostulante", mappedBy="id")
     */
    protected $idiomaPostulantes;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idioma
     *
     * @param string $idioma
     * @return idioma
     */
    public function setIdioma($idioma)
    {
        $this->idioma = $idioma;
    
        return $this;
    }

    /**
     * Get idioma
     *
     * @return string 
     */
    public function getIdioma()
    {
        return $this->idioma;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->curriculum = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add curriculum
     *
     * @param \tpare\DefaultBundle\Entity\curriculum $curriculum
     * @return Idioma
     */
    public function addCurriculum(\tpare\DefaultBundle\Entity\curriculum $curriculum)
    {
        $this->curriculum[] = $curriculum;
    
        return $this;
    }

    /**
     * Remove curriculum
     *
     * @param \tpare\DefaultBundle\Entity\curriculum $curriculum
     */
    public function removeCurriculum(\tpare\DefaultBundle\Entity\curriculum $curriculum)
    {
        $this->curriculum->removeElement($curriculum);
    }

    /**
     * Get curriculum
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCurriculum()
    {
        return $this->curriculum;
    }

    /**
     * Add idiomaPostulantes
     *
     * @param \tpare\DefaultBundle\Entity\idiomaPostulante $idiomaPostulantes
     * @return Idioma
     */
    public function addIdiomaPostulante(\tpare\DefaultBundle\Entity\idiomaPostulante $idiomaPostulantes)
    {
        $this->idiomaPostulantes[] = $idiomaPostulantes;
    
        return $this;
    }

    /**
     * Remove idiomaPostulantes
     *
     * @param \tpare\DefaultBundle\Entity\idiomaPostulante $idiomaPostulantes
     */
    public function removeIdiomaPostulante(\tpare\DefaultBundle\Entity\idiomaPostulante $idiomaPostulantes)
    {
        $this->idiomaPostulantes->removeElement($idiomaPostulantes);
    }

    /**
     * Get idiomaPostulantes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getIdiomaPostulantes()
    {
        return $this->idiomaPostulantes;
    }
}