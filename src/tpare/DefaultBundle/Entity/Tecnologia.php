<?php

namespace tpare\DefaultBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Un requerimiento va a tener una teconología, por ejemplo: Java, .PHP, MySQL, etc.
 *
 * @author ale
 * 
 * 
 * @ORM\Entity
 * @ORM\Table(name="tecnologias")
 */
class Tecnologia {
    
    /**
    *@ORM\Id
    *@ORM\Column(type="integer")
    *@ORM\GeneratedValue(strategy="AUTO") 
    */
    protected $id;
    
    /**
    *@ORM\Column(type="string", length=100)
    *@Assert\Length(max="50", maxMessage = "El nombre de la es demasiado largo.")
    *@Assert\NotBlank(message = "Debe ingresar un rol.") 
    */
    protected $nombre;
    
    /**
    *@ORM\Column(type="string", length=200)
    *@Assert\Length(max="400", maxMessage="La descripción es demasiado larga.")
    *@Assert\NotBlank(message = "Debe ingresar una descripción del rol.")
    */
    protected $descripcion;

    /**
     * @ORM\OneToMany(targetEntity="Conocimiento", mappedBy="curriculum")
     */
    protected $conocimientos;//Conocimiento
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Tecnologia
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Tecnologia
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    
        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->conocimientos = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add conocimientos
     *
     * @param \tpare\DefaultBundle\Entity\conocimiento $conocimientos
     * @return Tecnologia
     */
    public function addConocimiento(\tpare\DefaultBundle\Entity\conocimiento $conocimientos)
    {
        $this->conocimientos[] = $conocimientos;
    
        return $this;
    }

    /**
     * Remove conocimientos
     *
     * @param \tpare\DefaultBundle\Entity\conocimiento $conocimientos
     */
    public function removeConocimiento(\tpare\DefaultBundle\Entity\conocimiento $conocimientos)
    {
        $this->conocimientos->removeElement($conocimientos);
    }

    /**
     * Get conocimientos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getConocimientos()
    {
        return $this->conocimientos;
    }
}