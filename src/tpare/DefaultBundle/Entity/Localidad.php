<?php

namespace tpare\DefaultBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *@ORM\Entity 
 *@ORM\Table(name="localidades")
 * 
 *@author ale
 */
class Localidad {
    /**
     *@ORM\Id
     *@ORM\Column(type="integer")
     *@ORM\GeneratedValue(strategy="AUTO")  
     */
    protected $id;
    
    /**
     *@ORM\Column(type="string", length=100)
     */
    protected $nombre;
    
    /**
     *@ORM\ManyToOne(targetEntity="Provincia", inversedBy="localidades")
     *@ORM\JoinColumn(name="provincia_id", referencedColumnName="id")
     */
    protected $provincia;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Localidad
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set provincia
     *
     * @param \tpare\DefaultBundle\Entity\Provincia $provincia
     * @return Localidad
     */
    public function setProvincia(\tpare\DefaultBundle\Entity\Provincia $provincia = null)
    {
        $this->provincia = $provincia;

        return $this;
    }

    /**
     * Get provincia
     *
     * @return \tpare\DefaultBundle\Entity\Provincia 
     */
    public function getProvincia()
    {
        return $this->provincia;
    }
}