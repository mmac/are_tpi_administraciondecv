<?php

namespace tpare\DefaultBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * Repositorio de las Ofertas Laborales
 *
 * @author ale
 */
class OfertaLaboralRepository extends EntityRepository{
    
    public function buscarUltimasActivas(){
        $date = new \DateTime();
        $fechaActual = $date->format('Y-m-d');
        
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $qb->select('o')->from('DefaultBundle:OfertaLaboral', 'o');
        
        $qb->where('o.fechaInicio <= :fechaActual')
                ->setParameter('fechaActual', $fechaActual);
        $qb->andwhere('o.fechaFin >= :fechaActual')
                ->setParameter('fechaActual', $fechaActual);
            
        $qb->orderBy('o.fechaInicio', 'DESC');
        
        return $qb->getQuery()->getResult();
    }
    
    public function filtrarOfertas($tecnologiaFilters, $rolFilters, $jerarquiaFilters){
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $qb->select('o')   
           ->from('DefaultBundle:OfertaLaboral', 'o')
           ->leftJoin('DefaultBundle:Requerimiento', 'r', 'WITH', 'r.oferta = o.id')    
           ->leftJoin('DefaultBundle:Tecnologia', 't', 'WITH', 't.id = r.tecnologia');        
        
        if($rolFilters != null){
            $qb ->orWhere($qb->expr()->in('o.rol', $rolFilters));
        }
        
        if($jerarquiaFilters != null){
            $qb ->orWhere($qb->expr()->in('o.jerarquia', $jerarquiaFilters));
        }
        
        if($tecnologiaFilters != null){
            $qb ->orWhere($qb->expr()->in('t.id', $tecnologiaFilters));
        }
               
        return $qb->getQuery()->getResult();
    }
}

?>
