<?php

namespace tpare\DefaultBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Esta clase contiene los datos de la oferta laborar que va a dar de alta un encargado de RRHH
 * de la empresa Acme para que luego los postulantes puedan aplicar a dicha oferta.
 *
 * @ORM\Entity(repositoryClass="tpare\DefaultBundle\Entity\OfertaLaboralRepository")
 * @ORM\Table(name="ofertas_laborales")
 * @ORM\HasLifecycleCallbacks()
 * 
 * @author ale
 */
class OfertaLaboral {
    
    /**
     *@ORM\Id
     *@ORM\Column(type="integer")
     *@ORM\GeneratedValue(strategy="AUTO") 
    */
    protected $id;
    
    /**
     *@ORM\Column(type="string", length=70) 
     *@Assert\NotBlank(message = "Debe ingresar un titulo para la oferta laboral.")
    */
    protected $titulo;
    
    /**
    * @ORM\ManyToOne(targetEntity="Rol")
    * @ORM\JoinColumn(name="rol_id", referencedColumnName="id")
    * @Assert\NotBlank(message = "Debe seleccionar un rol.")
    */
    protected $rol;
    
    /**
     * @ORM\Column(type="string", length=50)
     * 
     * Validation annotations...
     * @Assert\Choice(
     *  callback = "getJerarquiaTypesCodes",
     *  message = "El tipo de jerarquía seleccionado no es válido."
     * )
     */
    protected $jerarquia;
    
     /**
     * @ORM\Column(type="string", length=30)
     * 
     * Validation annotations...
     * @Assert\Choice(
     *  callback = "getJornadaTypesCodes",
     *  message = "El tipo de jornada seleccionada no es válido."
     * )
     */
    protected $jornada;
    
     /**
     *@ORM\Column(type="integer")
     *@Assert\NotBlank(message = "Debe ingresar la cantidad de vacantes.")
     *@Assert\Type(type="integer")
     *@Assert\Range(
     *      min = 1,
     *      minMessage = "El número de vacantes no puede ser menor que 1.") 
     */
    protected $vacantes;
    
    /**
     *@ORM\Column(type="string", length=300) 
     *@Assert\NotBlank(message = "Debe ingresar una descripción para el puesto.")
    */
    protected $descripcion;
    
    /**
     *@ORM\Column(type="date")
     *@Assert\NotBlank(message = "Debe ingresar una fecha de inicio de la oferta laboral.") 
     */
    protected $fechaInicio;
    
    /**
     *@ORM\Column(type="date")
     *@Assert\NotBlank(message = "Debe ingresar una fecha de fin de la oferta laboral.") 
     */
    protected $fechaFin;
    
    /**
    * @ORM\ManyToOne(targetEntity="Localidad")
    * @ORM\JoinColumn(name="localidad_id", referencedColumnName="id")
    * @Assert\NotBlank(message = "Debe seleccionar una localidad.")
    */
    protected $localidad;
    
    /**
    * @ORM\ManyToOne(targetEntity="Provincia")
    * @ORM\JoinColumn(name="provincia_id", referencedColumnName="id")
    * @Assert\NotBlank(message = "Debe seleccionar una provincia.")
    */
    protected $provincia;
   
    /**
     * @ORM\OneToMany(targetEntity="Requerimiento", mappedBy="oferta", cascade={"persist"})
     * @Assert\Valid(traverse=true)
     **/
    protected $requerimientos;
    
    /**
    * @ORM\ManyToOne(targetEntity="User")
    * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
    */
    protected $user;
    
     /**
      * @ORM\ManyToOne(targetEntity="Idioma")
      * @ORM\JoinColumn(name="idioma_id", referencedColumnName="id")
      */
    protected $idioma;
     
    /**
     * @ORM\OneToMany(targetEntity="Aplicacion", mappedBy="ofertaLaboral")
     **/
    protected $aplicaciones; 
    
    
    public static function getJerarquiaTypes()
    {
        return array (
          'JUNIOR'    => "Junior",
          'SEMISENIOR' => "Semi Senior",
          'SENIOR' => "Senior",
        );
    }
    
    public static function getJerarquiaTypesCodes()
    {
        return array_keys(self::getJerarquiaTypes());
    }
    
    public static function getJornadaTypes()
    {
        return array (
          'PARCIAL'    => "Parcial",
          'COMPLETA' => "Completa",
        );
    }
    
    public static function getJornadaTypesCodes()
    {
        return array_keys(self::getJornadaTypes());
    }
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     * @return OfertaLaboral
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;
    
        return $this;
    }

    /**
     * Get titulo
     *
     * @return string 
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set jerarquia
     *
     * @param string $jerarquia
     * @return OfertaLaboral
     */
    public function setJerarquia($jerarquia)
    {
        $this->jerarquia = $jerarquia;
    
        return $this;
    }

    /**
     * Get jerarquia
     *
     * @return string 
     */
    public function getJerarquia()
    {
        return $this->jerarquia;
    }

    /**
     * Set vacantes
     *
     * @param integer $vacantes
     * @return OfertaLaboral
     */
    public function setVacantes($vacantes)
    {
        $this->vacantes = $vacantes;
    
        return $this;
    }

    /**
     * Get vacantes
     *
     * @return integer 
     */
    public function getVacantes()
    {
        return $this->vacantes;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return OfertaLaboral
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    
        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set fechaInicio
     *
     * @param \DateTime $fechaInicio
     * @return OfertaLaboral
     */
    public function setFechaInicio($fechaInicio)
    {
        $this->fechaInicio = $fechaInicio;
    
        return $this;
    }

    /**
     * Get fechaInicio
     *
     * @return \DateTime 
     */
    public function getFechaInicio()
    {
        return $this->fechaInicio;
    }

    /**
     * Set fechaFin
     *
     * @param \DateTime $fechaFin
     * @return OfertaLaboral
     */
    public function setFechaFin($fechaFin)
    {
        $this->fechaFin = $fechaFin;
    
        return $this;
    }

    /**
     * Get fechaFin
     *
     * @return \DateTime 
     */
    public function getFechaFin()
    {
        return $this->fechaFin;
    }


    /**
     * Set localidad
     *
     * @param \tpare\DefaultBundle\Entity\Localidad $localidad
     * @return OfertaLaboral
     */
    public function setLocalidad(\tpare\DefaultBundle\Entity\Localidad $localidad = null)
    {
        $this->localidad = $localidad;
    
        return $this;
    }

    /**
     * Get localidad
     *
     * @return \tpare\DefaultBundle\Entity\Localidad 
     */
    public function getLocalidad()
    {
        return $this->localidad;
    }

    /**
     * Set rol
     *
     * @param \tpare\DefaultBundle\Entity\Rol $rol
     * @return OfertaLaboral
     */
    public function setRol(\tpare\DefaultBundle\Entity\Rol $rol = null)
    {
        $this->rol = $rol;
    
        return $this;
    }

    /**
     * Get rol
     *
     * @return \tpare\DefaultBundle\Entity\Rol 
     */
    public function getRol()
    {
        return $this->rol;
    }

    /**
     * Set provincia
     *
     * @param \tpare\DefaultBundle\Entity\Provincia $provincia
     * @return OfertaLaboral
     */
    public function setProvincia(\tpare\DefaultBundle\Entity\Provincia $provincia = null)
    {
        $this->provincia = $provincia;
    
        return $this;
    }

    /**
     * Get provincia
     *
     * @return \tpare\DefaultBundle\Entity\Provincia 
     */
    public function getProvincia()
    {
        return $this->provincia;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->requerimientos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->aplicaciones = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add requerimientos
     *
     * @param \tpare\DefaultBundle\Entity\Requerimiento $requerimientos
     * @return OfertaLaboral
     */
    public function addRequerimiento(\tpare\DefaultBundle\Entity\Requerimiento $requerimientos)
    {
        $requerimientos->setOferta($this);
        $this->requerimientos->add($requerimientos);
        
        return $this;
    }
    
    /**
     * Remove requerimientos
     *
     * @param \tpare\DefaultBundle\Entity\Requerimiento $requerimientos
     */
    public function removeRequerimiento(\tpare\DefaultBundle\Entity\Requerimiento $requerimientos)
    {
        $this->requerimientos->removeElement($requerimientos);
    }

    /**
     * Get requerimientos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRequerimientos()
    {
        return $this->requerimientos;
    }

    /**
     * Set jornada
     *
     * @param string $jornada
     * @return OfertaLaboral
     */
    public function setJornada($jornada)
    {
        $this->jornada = $jornada;
    
        return $this;
    }

    /**
     * Get jornada
     *
     * @return string 
     */
    public function getJornada()
    {
        return $this->jornada;
    }

    /**
     * Set user
     *
     * @param \tpare\DefaultBundle\Entity\User $user
     * @return OfertaLaboral
     */
    public function setUser(\tpare\DefaultBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \tpare\DefaultBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set idioma
     *
     * @param \tpare\DefaultBundle\Entity\Idioma $idioma
     * @return OfertaLaboral
     */
    public function setIdioma(\tpare\DefaultBundle\Entity\Idioma $idioma = null)
    {
        $this->idioma = $idioma;
    
        return $this;
    }

    /**
     * Get idioma
     *
     * @return \tpare\DefaultBundle\Entity\Idioma 
     */
    public function getIdioma()
    {
        return $this->idioma;
    }

    /**
     * Add aplicaciones
     *
     * @param \tpare\DefaultBundle\Entity\Aplicacion $aplicaciones
     * @return OfertaLaboral
     */
    public function addAplicacione(\tpare\DefaultBundle\Entity\Aplicacion $aplicaciones)
    {
        $this->aplicaciones[] = $aplicaciones;
    
        return $this;
    }

    /**
     * Remove aplicaciones
     *
     * @param \tpare\DefaultBundle\Entity\Aplicacion $aplicaciones
     */
    public function removeAplicacione(\tpare\DefaultBundle\Entity\Aplicacion $aplicaciones)
    {
        $this->aplicaciones->removeElement($aplicaciones);
    }

    /**
     * Get aplicaciones
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAplicaciones()
    {
        return $this->aplicaciones;
    }
}