<?php
namespace tpare\DefaultBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Esta clase contiene los datos personales de un curriculum
 *
 * @ORM\Table(name="datosPersonales")
 * @ORM\Entity
 * 
 */
class DatosPersonales {
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * /**
     *@ORM\Column(type="string", length=70) 
     *@Assert\NotBlank(message = "Debe ingresar el nombre.")
    */
    protected $nombre;

    /**
     *@ORM\Column(type="string", length=70) 
     *@Assert\NotBlank(message = "Debe ingresar el apellido.")
    */
    protected $apellido;
    
    /**
     *@ORM\Column(type="date")
     *@Assert\NotBlank(message = "Debe ingresar una fecha de nacimiento.")
     * 
     */
    protected $fechaNacimiento;
    
    /**
     * @ORM\Column(type="string", length=30)
     * @Assert\NotBlank(message = "Debe seleccionar un estado civil")
     * 
     * Validation annotations...
     * @Assert\Choice(
     *  callback = "getSexosCodes",
     *  message = "El estado civil seleccionado no es válido."
     * )
     */
    protected $sexo;
    
    public static function getSexoTypes()
    {
        return array (
          'MASCULINO'    => "Masculino",
          'FEMENINO' => "Femenino",       
        );
    }
    
    public static function getSexosCodes()
    {
        return array_keys(self::getSexoTypes());
    } 
    
    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     * 
     */
    protected $nacionalidad;
    
    /**
     * @ORM\Column(type="string", length=30)
     * @Assert\NotBlank(message = "Debe seleccionar un estado civil")
     * 
     * Validation annotations...
     * @Assert\Choice(
     *  callback = "getEstadosCivilesCodes",
     *  message = "El estado civil seleccionado no es válido."
     * )
     */
    protected $estadoCivil;
    
    public static function getEstadosCiviles()
    {
        return array (
          'SOLTERO'    => "Soltero",
          'CASADO' => "Casado",
          'DIVORCIADO'=>"Divorciado",
          'VIUDO' => "Viudo",
          
        );
    }
    
    public static function getEstadosCivilesCodes()
    {
        return array_keys(self::getEstadosCiviles());
    }
    
    
    /**
     *@ORM\Column(type="integer")
     *@Assert\NotBlank(message = "Debe ingresar la cantidad de hijos.")
     * @Assert\Range(
     *      min = 0,
     *      minMessage = "El número de hijos no puede ser menor que 0.") 
     */
    protected $hijos;
    
    
    /**
    * @ORM\ManyToOne(targetEntity="Provincia")
    * @ORM\JoinColumn(name="provincia_id", referencedColumnName="id")
    * @Assert\NotBlank(message = "Debe seleccionar una provincia.")
    */
    protected $provincia;
    
    /**
    * @ORM\ManyToOne(targetEntity="Localidad")
    * @ORM\JoinColumn(name="localidad_id", referencedColumnName="id")
    * @Assert\NotBlank(message = "Debe seleccionar una localidad.")
    */
    protected $localidad;
    
    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     * 
     */
    protected $calle;
    
    /**
     * @ORM\Column(type="integer", nullable=true)
     * 
     */
    protected $numeroCalle;
    
    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     * 
     */
    protected $telefono1;
    
    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     * 
     */
    protected $telefono2;
    
    /**
     * @ORM\Column(type="string", length=60)
     * @Assert\NotBlank(message = "Debe ingresar una dirección de email.")
     * @Assert\Email(
     *     message = "El email '{{ value }}' no es válido.",
     *     checkMX = true
     * )
     */
    protected $email;
    
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return DatosPersonales
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set apellido
     *
     * @param string $apellido
     * @return DatosPersonales
     */
    public function setApellido($apellido)
    {
        $this->apellido = $apellido;
    
        return $this;
    }

    /**
     * Get apellido
     *
     * @return string 
     */
    public function getApellido()
    {
        return $this->apellido;
    }

    /**
     * Set fechaNacimiento
     *
     * @param \DateTime $fechaNacimiento
     * @return DatosPersonales
     */
    public function setFechaNacimiento($fechaNacimiento)
    {
        $this->fechaNacimiento = $fechaNacimiento;
    
        return $this;
    }

    /**
     * Get fechaNacimiento
     *
     * @return \DateTime 
     */
    public function getFechaNacimiento()
    {
        return $this->fechaNacimiento;
    }

    /**
     * Set sexo
     *
     * @param string $sexo
     * @return DatosPersonales
     */
    public function setSexo($sexo)
    {
        $this->sexo = $sexo;
    
        return $this;
    }

    /**
     * Get sexo
     *
     * @return string 
     */
    public function getSexo()
    {
        return $this->sexo;
    }

    /**
     * Set estadoCivil
     *
     * @param string $estadoCivil
     * @return DatosPersonales
     */
    public function setEstadoCivil($estadoCivil)
    {
        $this->estadoCivil = $estadoCivil;
    
        return $this;
    }

    /**
     * Get estadoCivil
     *
     * @return string 
     */
    public function getEstadoCivil()
    {
        return $this->estadoCivil;
    }

    /**
     * Set hijos
     *
     * @param integer $hijos
     * @return DatosPersonales
     */
    public function setHijos($hijos)
    {
        $this->hijos = $hijos;
    
        return $this;
    }

    /**
     * Get hijos
     *
     * @return integer 
     */
    public function getHijos()
    {
        return $this->hijos;
    }

    /**
     * Set calle
     *
     * @param string $calle
     * @return DatosPersonales
     */
    public function setCalle($calle)
    {
        $this->calle = $calle;
    
        return $this;
    }

    /**
     * Get calle
     *
     * @return string 
     */
    public function getCalle()
    {
        return $this->calle;
    }

    /**
     * Set numeroCalle
     *
     * @param integer $numeroCalle
     * @return DatosPersonales
     */
    public function setNumeroCalle($numeroCalle)
    {
        $this->numeroCalle = $numeroCalle;
    
        return $this;
    }

    /**
     * Get numeroCalle
     *
     * @return integer 
     */
    public function getNumeroCalle()
    {
        return $this->numeroCalle;
    }

    /**
     * Set telefono1
     *
     * @param string $telefono1
     * @return DatosPersonales
     */
    public function setTelefono1($telefono1)
    {
        $this->telefono1 = $telefono1;
    
        return $this;
    }

    /**
     * Get telefono1
     *
     * @return string 
     */
    public function getTelefono1()
    {
        return $this->telefono1;
    }

    /**
     * Set telefono2
     *
     * @param string $telefono2
     * @return DatosPersonales
     */
    public function setTelefono2($telefono2)
    {
        $this->telefono2 = $telefono2;
    
        return $this;
    }

    /**
     * Get telefono2
     *
     * @return string 
     */
    public function getTelefono2()
    {
        return $this->telefono2;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return DatosPersonales
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set provincia
     *
     * @param \tpare\DefaultBundle\Entity\Provincia $provincia
     * @return DatosPersonales
     */
    public function setProvincia(\tpare\DefaultBundle\Entity\Provincia $provincia = null)
    {
        $this->provincia = $provincia;
    
        return $this;
    }

    /**
     * Get provincia
     *
     * @return \tpare\DefaultBundle\Entity\Provincia 
     */
    public function getProvincia()
    {
        return $this->provincia;
    }

    /**
     * Set localidad
     *
     * @param \tpare\DefaultBundle\Entity\Localidad $localidad
     * @return DatosPersonales
     */
    public function setLocalidad(\tpare\DefaultBundle\Entity\Localidad $localidad = null)
    {
        $this->localidad = $localidad;
    
        return $this;
    }

    /**
     * Get localidad
     *
     * @return \tpare\DefaultBundle\Entity\Localidad 
     */
    public function getLocalidad()
    {
        return $this->localidad;
    }

    /**
     * Set nacionalidad
     *
     * @param string $nacionalidad
     * @return DatosPersonales
     */
    public function setNacionalidad($nacionalidad)
    {
        $this->nacionalidad = $nacionalidad;
    
        return $this;
    }

    /**
     * Get nacionalidad
     *
     * @return string 
     */
    public function getNacionalidad()
    {
        return $this->nacionalidad;
    }
}