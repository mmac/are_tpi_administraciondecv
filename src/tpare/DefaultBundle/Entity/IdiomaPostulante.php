<?php
namespace tpare\DefaultBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 *
 * @ORM\Table(name="idiomaPostulante")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class IdiomaPostulante {
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
     protected $id;

     /**
      *
      * @ORM\ManyToOne(targetEntity="Idioma", inversedBy="idiomaPostulantes")
      * @ORM\JoinColumn(name="idioma_id", referencedColumnName="id")
      * @Assert\NotBlank(message="Debe seleccionar un idioma.")
      */
     protected $idioma;

     /**
     *@ORM\ManyToOne(targetEntity="Curriculum", inversedBy="idiomas")
     *@ORM\JoinColumn(name="curriculum_id", referencedColumnName="id")
     */
     protected $curriculum;

     /**
      * 
     * @ORM\OneToMany(targetEntity="CertificadoIdioma", mappedBy="idioma", cascade={"persist"})
      * @Assert\Valid(traverse=true)
     */
    protected $certificados;
     
    /**
    *
    * @ORM\Column(type="string", length=50)
     * Validation annotations...
     * @Assert\NotBlank(message="Debe seleccionar un nivel.")
     * @Assert\Choice(
     *  callback = "getNivelesCodes",
     *  message = "El nivel seleccionado no es válido."
     * )
    */
     protected $nivelLectura;     // NivelGenerico

     /**
      *     @ORM\Column(type="string", length=50)
      * Validation annotations...
      * @Assert\NotBlank(message="Debe seleccionar un nivel.")
     * @Assert\Choice(
     *  callback = "getNivelesCodes",
     *  message = "El nivel seleccionado no es válido."
     * )
      */
     protected $nivelOral;        // NivelGenerico 
     
     /**
      *     @ORM\Column(type="string", length=50)
      * Validation annotations...
      * @Assert\NotBlank(message="Debe seleccionar un nivel.")
     * @Assert\Choice(
     *  callback = "getNivelesCodes",
     *  message = "El nivel seleccionado no es válido."
     * )
      */
     protected $nivelEscrito;     // NivelGenerico
     
     /**
      *     @ORM\Column(type="string", length=50)
      * Validation annotations...
      * @Assert\NotBlank(message="Debe seleccionar un nivel.")
     * @Assert\Choice(
     *  callback = "getNivelesCodes",
     *  message = "El nivel seleccionado no es válido."
     * )
      */
     protected $nivelComprension; // NivelGenerico
     
     
     /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idioma
     *
     * @param \tpare\DefaultBundle\Entity\idioma $idioma
     * @return idiomaPostulante
     */
    public function setIdioma(\tpare\DefaultBundle\Entity\idioma $idioma = null)
    {
        $this->idioma = $idioma;
    
        return $this;
    }

    /**
     * Get idioma
     *
     * @return \tpare\DefaultBundle\Entity\idioma 
     */
    public function getIdioma()
    {
        return $this->idioma;
    }

    /**
     * Set curriculum
     *
     * @param \tpare\DefaultBundle\Entity\curriculum $curriculum
     * @return IdiomaPostulante
     */
    public function setCurriculum(\tpare\DefaultBundle\Entity\curriculum $curriculum = null)
    {
        $this->curriculum = $curriculum;
    
        return $this;
    }

    /**
     * Get curriculum
     *
     * @return \tpare\DefaultBundle\Entity\curriculum 
     */
    public function getCurriculum()
    {
        return $this->curriculum;
    }



    /**
     * Set nivelLectura
     *
     * @param string $nivelLectura
     * @return IdiomaPostulante
     */
    public function setNivelLectura($nivelLectura)
    {
        $this->nivelLectura = $nivelLectura;
    
        return $this;
    }

    /**
     * Get nivelLectura
     *
     * @return string 
     */
    public function getNivelLectura()
    {
        return $this->nivelLectura;
    }

    /**
     * Set nivelOral
     *
     * @param string $nivelOral
     * @return IdiomaPostulante
     */
    public function setNivelOral($nivelOral)
    {
        $this->nivelOral = $nivelOral;
    
        return $this;
    }

    /**
     * Get nivelOral
     *
     * @return string 
     */
    public function getNivelOral()
    {
        return $this->nivelOral;
    }

    /**
     * Set nivelEscrito
     *
     * @param string $nivelEscrito
     * @return IdiomaPostulante
     */
    public function setNivelEscrito($nivelEscrito)
    {
        $this->nivelEscrito = $nivelEscrito;
    
        return $this;
    }

    /**
     * Get nivelEscrito
     *
     * @return string 
     */
    public function getNivelEscrito()
    {
        return $this->nivelEscrito;
    }

    /**
     * Set nivelComprension
     *
     * @param string $nivelComprension
     * @return IdiomaPostulante
     */
    public function setNivelComprension($nivelComprension)
    {
        $this->nivelComprension = $nivelComprension;
    
        return $this;
    }

    /**
     * Get nivelComprension
     *
     * @return string 
     */
    public function getNivelComprension()
    {
        return $this->nivelComprension;
    }

    /**
     * Set fechaInicio
     *
     * @param \DateTime $fechaInicio
     * @return IdiomaPostulante
     */
    public function setFechaInicio($fechaInicio)
    {
        $this->fechaInicio = $fechaInicio;
    
        return $this;
    }

    /**
     * Get fechaInicio
     *
     * @return \DateTime 
     */
    public function getFechaInicio()
    {
        return $this->fechaInicio;
    }

    /**
     * Set fechaFin
     *
     * @param \DateTime $fechaFin
     * @return IdiomaPostulante
     */
    public function setFechaFin($fechaFin)
    {
        $this->fechaFin = $fechaFin;
    
        return $this;
    }

    /**
     * Get fechaFin
     *
     * @return \DateTime 
     */
    public function getFechaFin()
    {
        return $this->fechaFin;
    }

    /**
     * Set ultimaVezPracticado
     *
     * @param \DateTime $ultimaVezPracticado
     * @return IdiomaPostulante
     */
    public function setUltimaVezPracticado($ultimaVezPracticado)
    {
        $this->ultimaVezPracticado = $ultimaVezPracticado;
    
        return $this;
    }

    /**
     * Get ultimaVezPracticado
     *
     * @return \DateTime 
     */
    public function getUltimaVezPracticado()
    {
        return $this->ultimaVezPracticado;
    }
    
    public static function getNiveles()
    {
        return array (
          'BASICO'    => "Basico",
          'MEDIO' => "Medio",
          'ALTO' => "Alto",
        );
    }
    
    public static function getNivelesCodes()
    {
        return array_keys(self::getNiveles());
    }
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->certificados = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add certificados
     *
     * @param \tpare\DefaultBundle\Entity\CertificadoIdioma $certificados
     * @return IdiomaPostulante
     */
    public function addCertificado(\tpare\DefaultBundle\Entity\CertificadoIdioma $certificados)
    {
        
        $certificados->setIdioma($this);
        $this->certificados->add($certificados);
    
        return $this;
    }

    /**
     * Remove certificados
     *
     * @param \tpare\DefaultBundle\Entity\CertificadoIdioma $certificados
     */
    public function removeCertificado(\tpare\DefaultBundle\Entity\CertificadoIdioma $certificados)
    {
        $certificados->setIdioma(null);
        $this->certificados->removeElement($certificados);
    }

    /**
     * Get certificados
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCertificados()
    {
        return $this->certificados;
    }
}