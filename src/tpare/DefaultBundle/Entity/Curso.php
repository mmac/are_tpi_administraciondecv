<?php
namespace tpare\DefaultBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Description of Curso
 *
 * @author ale
 * @ORM\Table(name="cursos")
 * @ORM\Entity
 */
class Curso {
    
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\Column(type="string", length=50)
     * @Assert\NotBlank(message = "Debe ingresar un titulo para el estudio.")
     */
    protected $titulo; 
    
    /**
     * @ORM\Column(type="string", length=50)
     * @Assert\NotBlank(message = "Debe ingresar una institución para el estudio.")
     */
    protected $institucion;
    
    /**
     *@ORM\Column(type="integer")
     *@Assert\NotBlank(message = "Debe ingresar la cantidad de horas del curso.")
     *@Assert\Type(type="integer")
     *@Assert\Range(
     *      min = 1,
     *      minMessage = "El número de horas no puede ser menor que 1.")
     */
    protected $duracion;
    
    /**
     *
     * @ORM\Column(type="string", nullable=true )
     */
    protected $descripcion;
    
    /**
     *@ORM\Column(type="date")
     *@Assert\NotBlank(message="Debe ingresar el año de finalización del curso.")
     */
    protected $fechaFin;
    
    /**
     *@ORM\ManyToOne(targetEntity="Curriculum", inversedBy="cursos")
     *@ORM\JoinColumn(name="curriculum_id", referencedColumnName="id")
     */
    protected $curriculum;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     * @return Curso
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;
    
        return $this;
    }

    /**
     * Get titulo
     *
     * @return string 
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set institucion
     *
     * @param string $institucion
     * @return Curso
     */
    public function setInstitucion($institucion)
    {
        $this->institucion = $institucion;
    
        return $this;
    }

    /**
     * Get institucion
     *
     * @return string 
     */
    public function getInstitucion()
    {
        return $this->institucion;
    }

    /**
     * Set duración
     *
     * @param integer $duración
     * @return Curso
     */
    public function setDuración($duración)
    {
        $this->duración = $duración;
    
        return $this;
    }

    /**
     * Get duración
     *
     * @return integer 
     */
    public function getDuración()
    {
        return $this->duración;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Curso
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    
        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set curriculum
     *
     * @param \tpare\DefaultBundle\Entity\Curriculum $curriculum
     * @return Curso
     */
    public function setCurriculum(\tpare\DefaultBundle\Entity\Curriculum $curriculum = null)
    {
        $this->curriculum = $curriculum;
    
        return $this;
    }

    /**
     * Get curriculum
     *
     * @return \tpare\DefaultBundle\Entity\Curriculum 
     */
    public function getCurriculum()
    {
        return $this->curriculum;
    }

    /**
     * Set duracion
     *
     * @param integer $duracion
     * @return Curso
     */
    public function setDuracion($duracion)
    {
        $this->duracion = $duracion;
    
        return $this;
    }

    /**
     * Get duracion
     *
     * @return integer 
     */
    public function getDuracion()
    {
        return $this->duracion;
    }

    /**
     * Set añoFin
     *
     * @param \DateTime $añoFin
     * @return Curso
     */
    public function setAñoFin($añoFin)
    {
        $this->añoFin = $añoFin;
    
        return $this;
    }

    /**
     * Get añoFin
     *
     * @return \DateTime 
     */
    public function getAñoFin()
    {
        return $this->añoFin;
    }

    /**
     * Set anoFin
     *
     * @param string $anoFin
     * @return Curso
     */
    public function setAnoFin($anoFin)
    {
        $this->anoFin = $anoFin;
    
        return $this;
    }

    /**
     * Get anoFin
     *
     * @return string 
     */
    public function getAnoFin()
    {
        return $this->anoFin;
    }

    /**
     * Set fechaFin
     *
     * @param \DateTime $fechaFin
     * @return Curso
     */
    public function setFechaFin($fechaFin)
    {
        $this->fechaFin = $fechaFin;
    
        return $this;
    }

    /**
     * Get fechaFin
     *
     * @return \DateTime 
     */
    public function getFechaFin()
    {
        return $this->fechaFin;
    }
}