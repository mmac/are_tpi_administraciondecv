<?php
namespace tpare\DefaultBundle\Entity\Filter;
use Doctrine\Common\Collections\ArrayCollection;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CurriculumFilter
 *
 * @author pmarrone
 */
class CVFilter {
    protected $requerimientos;
    protected $idiomas;
    protected $tituloMinimo;
    protected $palabrasClaveTitulo;
    protected $tituloExcluyente;
    protected $idiomaExcluyente;
    protected $listarCVReqNoCumplidos;
    protected $sexo;
    protected $cargo;
    protected $anos;
    
    public function __construct()
    {
        $this->requerimientos = new ArrayCollection();
        $this->idiomas = new ArrayCollection();
        $this->palabrasClaveTitulo = new ArrayCollection();
    }
    
    public function getRequerimientos() {
        return $this->requerimientos;
    }

    public function setRequerimientos($requerimientos) {
        $this->requerimientos = $requerimientos;
    }

    
    public function getIdiomas() {
        return $this->idiomas;
    }

    public function setIdiomas($idiomas) {
        $this->idiomas = $idiomas;
    }

    public function getTituloMinimo() {
        return $this->tituloMinimo;
    }

    public function setTituloMinimo($tituloMinimo) {
        $this->tituloMinimo = $tituloMinimo;
    }

    public function getPalabrasClaveTitulo() {
        return $this->palabrasClaveTitulo;
    }

    public function setPalabrasClaveTitulo($palabrasClaveTitulo) {
        $this->palabrasClaveTitulo = $palabrasClaveTitulo;
    }
    
    public function getTituloExcluyente() {
        return $this->tituloExcluyente;
    }

    public function setTituloExcluyente($tituloExcluyente) {
        $this->tituloExcluyente = $tituloExcluyente;
    }

    public function getIdiomaExcluyente() {
        return $this->idiomaExcluyente;
    }

    public function setIdiomaExcluyente($idiomaExcluyente) {
        $this->idiomaExcluyente = $idiomaExcluyente;
    }

    public function getListarCVReqNoCumplidos() {
        return $this->listarCVReqNoCumplidos;
    }

    public function setListarCVReqNoCumplidos($listarCVReqNoCumplidos) {
        $this->listarCVReqNoCumplidos = $listarCVReqNoCumplidos;
    }

    public function getSexo() {
        return $this->sexo;
    }

    public function setSexo($sexo) {
        $this->sexo = $sexo;
    }

    public function getCargo() {
        return $this->cargo;
    }

    public function setCargo($cargo) {
        $this->cargo = $cargo;
    }

    public function getAnos() {
        return $this->anos;
    }

    public function setAnos($anos) {
        $this->anos = $anos;
    }

    /**
     * Add requerimientos
     *
     * @param \tpare\DefaultBundle\Entity\Requerimiento $requerimientos
     * @return OfertaLaboral
     */
    public function addRequerimiento(\tpare\DefaultBundle\Entity\Requerimiento $requerimiento)
    {        
        $this->requerimientos->add($requerimiento);        
        return $this;
    }
    
    /**
     * Remove requerimientos
     *
     * @param \tpare\DefaultBundle\Entity\Requerimiento $requerimientos
     */
    public function removeRequerimiento(\tpare\DefaultBundle\Entity\Requerimiento $requerimiento)
    {
        $this->requerimientos->removeElement($requerimiento);
    }
}

?>
