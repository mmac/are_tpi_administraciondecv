<?php

namespace tpare\DefaultBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Rol que se debe ejercer en dicho puesto. Ejemplo: programador, analista, arquitecto, etc.
 *
 * @author ale
 * 
 * 
 * @ORM\Entity
 * @ORM\Table(name="roles")
 */
class Rol {
    /**
    *@ORM\Id
    *@ORM\Column(type="integer")
    *@ORM\GeneratedValue(strategy="AUTO") 
    */
    protected $id;
    
    /**
    *@ORM\Column(type="string", length=100)
    *@Assert\Length(max="50", maxMessage = "El nombre del rol es demasiado largo.")
    *@Assert\NotBlank(message = "Debe ingresar un rol.") 
    */
    protected $nombre;
    
    /**
    *@ORM\Column(type="string", length=200)
    *@Assert\Length(max="400", maxMessage="La descripción es demasiado larga.")
    */
    protected $descripcion;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Rol
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Rol
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    
        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }
}