<?php
namespace tpare\DefaultBundle\Entity;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 *
 * @ORM\Table(name="certificadoIdioma")
 * @ORM\Entity
 * 
 */
class CertificadoIdioma {
    
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
      *
      * @ORM\ManyToOne(targetEntity="IdiomaPostulante", inversedBy="certificados")
      * @ORM\JoinColumn(name="idiomaPostulante_id", referencedColumnName="id", onDelete="CASCADE")
      * 
     * 
      */
    protected $idioma;
    
    /**
     * @ORM\Column(type="date", nullable=true)
     */
    protected $fechaEmision;
    
    
    
     /**
     *
     * @ORM\Column(type="string", length=70)
     */
     protected $institucion;    
     
     
     /**
     *
     * @ORM\Column(type="string", length=70, nullable=true)
     */
    protected $nombre;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fechaEmision
     *
     * @param \DateTime $fechaEmision
     * @return CertificadoIdioma
     */
    public function setFechaEmision($fechaEmision)
    {
        $this->fechaEmision = $fechaEmision;
    
        return $this;
    }

    /**
     * Get fechaEmision
     *
     * @return \DateTime 
     */
    public function getFechaEmision()
    {
        return $this->fechaEmision;
    }

    /**
     * Set institucion
     *
     * @param string $institucion
     * @return CertificadoIdioma
     */
    public function setInstitucion($institucion)
    {
        $this->institucion = $institucion;
    
        return $this;
    }

    /**
     * Get institucion
     *
     * @return string 
     */
    public function getInstitucion()
    {
        return $this->institucion;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return CertificadoIdioma
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set idioma
     *
     * @param \tpare\DefaultBundle\Entity\IdiomaPostulante $idioma
     * @return CertificadoIdioma
     */
    public function setIdioma(\tpare\DefaultBundle\Entity\IdiomaPostulante $idioma = null)
    {
        $this->idioma = $idioma;
    
        return $this;
    }

    /**
     * Get idioma
     *
     * @return \tpare\DefaultBundle\Entity\IdiomaPostulante 
     */
    public function getIdioma()
    {
        return $this->idioma;
    }
}