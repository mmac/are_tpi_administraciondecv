<?php

namespace tpare\DefaultBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="empresa")
 *
 * @author ale
 */
class Empresa {
    
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=25, unique=true)
     */
    private $nombre;
    
    /**
     * @ORM\Column(type="string", length=25, unique=true)
     */
    protected $pathLogo;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Empresa
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set pathLogo
     *
     * @param string $pathLogo
     * @return Empresa
     */
    public function setPathLogo($pathLogo)
    {
        $this->pathLogo = $pathLogo;
    
        return $this;
    }

    /**
     * Get pathLogo
     *
     * @return string 
     */
    public function getPathLogo()
    {
        return $this->pathLogo;
    }
}