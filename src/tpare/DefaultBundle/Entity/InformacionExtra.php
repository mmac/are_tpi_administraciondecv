<?php

namespace tpare\DefaultBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 *
 * @ORM\Table(name="infoExtra")
 * @ORM\Entity
 */
class InformacionExtra {
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\Column(type="string", length=400)
     * @Assert\NotBlank(message = "Debe ingresar un titulo para el estudio.")
     */
    protected $descripcion; 

    /**
     *@ORM\ManyToOne(targetEntity="Curriculum", inversedBy="informacionExtra")
     *@ORM\JoinColumn(name="curriculum_id", referencedColumnName="id")
     */
    protected $curriculum;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return InformacionExtra
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    
        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set curriculum
     *
     * @param \tpare\DefaultBundle\Entity\Curriculum $curriculum
     * @return InformacionExtra
     */
    public function setCurriculum(\tpare\DefaultBundle\Entity\Curriculum $curriculum = null)
    {
        $this->curriculum = $curriculum;
    
        return $this;
    }

    /**
     * Get curriculum
     *
     * @return \tpare\DefaultBundle\Entity\Curriculum 
     */
    public function getCurriculum()
    {
        return $this->curriculum;
    }
}