<?php
namespace tpare\DefaultBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Description of Aplicacion
 *
 * @author ale
 * 
 * @ORM\Entity()
 * @ORM\Table(name="aplicaciones")
 * @ORM\HasLifecycleCallbacks()
 */
class Aplicacion {
    
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     *@ORM\Column(type="date")
     */
    protected $fechaDeAplicacion;
    
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="aplicaciones")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     **/
    protected $user;
    
    /**
     * @ORM\ManyToOne(targetEntity="OfertaLaboral", inversedBy="aplicaciones")
     * @ORM\JoinColumn(name="oferta_id", referencedColumnName="id")
     */
    protected $ofertaLaboral;
    /**
     * Constructor
     */
    
    /**
     * Set fechaDeAplicacion
     *
     * @param \DateTime $fechaDeAplicacion
     * @return Aplicacion
     * 
     * @ORM\PrePersist
     */
    public function setFechaDeAplicacion()
    {
        $this->fechaDeAplicacion = new \DateTime();
    
        return $this;
    }

    /**
     * Get fechaDeAplicacion
     *
     * @return \DateTime 
     */
    public function getFechaDeAplicacion()
    {
        return $this->fechaDeAplicacion;
    }

    /**
     * Get user
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set ofertaLaboral
     *
     * @param \tpare\DefaultBundle\Entity\OfertaLaboral $ofertaLaboral
     * @return Aplicacion
     */
    public function setOfertaLaboral(\tpare\DefaultBundle\Entity\OfertaLaboral $ofertaLaboral = null)
    {
        $this->ofertaLaboral = $ofertaLaboral;
    
        return $this;
    }

    /**
     * Get ofertaLaboral
     *
     * @return \tpare\DefaultBundle\Entity\OfertaLaboral 
     */
    public function getOfertaLaboral()
    {
        return $this->ofertaLaboral;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param \tpare\DefaultBundle\Entity\User $user
     * @return Aplicacion
     */
    public function setUser(\tpare\DefaultBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }
}