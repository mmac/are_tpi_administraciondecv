<?php

namespace tpare\DefaultBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


/**
 *
 * @ORM\Table(name="users")
 * @ORM\Entity
 * @UniqueEntity(fields = {"username"}, message = "El nombre de usuario ya está en uso.") 
 * @ORM\HasLifecycleCallbacks()
 */
class User implements UserInterface, \Serializable
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=25, unique=true)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=32)
     */
    private $salt;

    /**
     * @ORM\Column(type="string", length=60)
     * @Assert\NotBlank(message = "Debe ingresar una contraseña.")
     * @Assert\Length(min = "6", minMessage="La contraseña debe tener como mínimo 6 caracteres.")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=60, unique=true)
     * @Assert\NotBlank(message = "Debe ingresar una dirección de email.")
     * @Assert\Email(
     *     message = "El email '{{ value }}' no es válido.",
     *     checkMX = true
     * )
     */
    private $email;
    
    /**
     * @ORM\Column(type="string", length=60)
     * @Assert\NotBlank(message = "Debe ingresar un nombre.")
     */
    protected $nombre;
    
    /**
     * @ORM\Column(type="string", length=60)
     * @Assert\NotBlank(message = "Debe ingresar un apellido.")
     */
    protected $apellido;
    
    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $telefono;
    
     /**
     *@ORM\Column(type="datetime")
     */
    protected $fechaAlta;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;
    
    
    /**
     * @ORM\ManyToMany(targetEntity="Role", inversedBy="users")
     *
     */
    private $roles;
    
     /**
     * @ORM\ManyToOne(targetEntity="Empresa")
     * @ORM\JoinColumn(name="empresa_id", referencedColumnName="id")
     **/
    protected $empresa;
    
    /**
     * @ORM\OneToOne(targetEntity="Curriculum")
     * @ORM\JoinColumn(name="curriculum_id", referencedColumnName="id")
     **/
    protected $curriculum;
    
    /**
     * @ORM\OneToMany(targetEntity="Aplicacion", mappedBy="user")
     **/
    protected $aplicaciones;

    public function __construct()
    {
        $this->isActive = true;
        $this->salt = md5(uniqid(null, true));
        $this->roles = new ArrayCollection();
        $this->aplicaciones = new ArrayCollection();
    }

    /**
     * @inheritDoc
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @inheritDoc
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * @inheritDoc
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @inheritDoc
     */
    public function getRoles()
    {
        return $this->roles->toArray();
    }

    /**
     * @inheritDoc
     */
    public function eraseCredentials()
    {
    }

    /**
     * @see \Serializable::serialize()
     */
    public function serialize()
    {
        return serialize(array(
            $this->id,
        ));
    }

    /**
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
        ) = unserialize($serialized);
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;
    
        return $this;
    }

    /**
     * Set salt
     *
     * @param string $salt
     * @return User
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
    
        return $this;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;
    
        return $this;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return User
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    
        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return User
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set apellido
     *
     * @param string $apellido
     * @return User
     */
    public function setApellido($apellido)
    {
        $this->apellido = $apellido;
    
        return $this;
    }

    /**
     * Get apellido
     *
     * @return string 
     */
    public function getApellido()
    {
        return $this->apellido;
    }

    /**
     * Set telefono
     *
     * @param integer $telefono
     * @return User
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;
    
        return $this;
    }

    /**
     * Get telefono
     *
     * @return integer 
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Cuando la entidad se tira al a BD se setea la fecha actual. Lo hace automaticamente.
     *
     * @param \DateTime $fechaSubida
     * @ORM\PrePersist
     */
    public function setFechaAlta()
    {
        $this->fechaAlta = new \DateTime();
    
        return $this;
    }

    /**
     * Get fechaAlta
     *
     * @return \DateTime 
     */
    public function getFechaAlta()
    {
        return $this->fechaAlta;
    }

    /**
     * Add roles
     *
     * @param \tpare\DefaultBundle\Entity\Role $roles
     * @return User
     */
    public function addRole(\tpare\DefaultBundle\Entity\Role $roles)
    {
        $this->roles[] = $roles;
    
        return $this;
    }

    /**
     * Remove roles
     *
     * @param \tpare\DefaultBundle\Entity\Role $roles
     */
    public function removeRole(\tpare\DefaultBundle\Entity\Role $roles)
    {
        $this->roles->removeElement($roles);
    }

    /**
     * Set empresa
     *
     * @param \tpare\DefaultBundle\Entity\Empresa $empresa
     * @return User
     */
    public function setEmpresa(\tpare\DefaultBundle\Entity\Empresa $empresa = null)
    {
        $this->empresa = $empresa;
    
        return $this;
    }

    /**
     * Get empresa
     *
     * @return \tpare\DefaultBundle\Entity\Empresa 
     */
    public function getEmpresa()
    {
        return $this->empresa;
    }

    /**
     * Set curriculum
     *
     * @param \tpare\DefaultBundle\Entity\Curriculum $curriculum
     * @return User
     */
    public function setCurriculum(\tpare\DefaultBundle\Entity\Curriculum $curriculum = null)
    {
        $this->curriculum = $curriculum;
    
        return $this;
    }

    /**
     * Get curriculum
     *
     * @return \tpare\DefaultBundle\Entity\Curriculum 
     */
    public function getCurriculum()
    {
        return $this->curriculum;
    }

    /**
     * Add aplicaciones
     *
     * @param \tpare\DefaultBundle\Entity\Aplicacion $aplicaciones
     * @return User
     */
    public function addAplicacione(\tpare\DefaultBundle\Entity\Aplicacion $aplicaciones)
    {
        $this->aplicaciones[] = $aplicaciones;
    
        return $this;
    }

    /**
     * Remove aplicaciones
     *
     * @param \tpare\DefaultBundle\Entity\Aplicacion $aplicaciones
     */
    public function removeAplicacione(\tpare\DefaultBundle\Entity\Aplicacion $aplicaciones)
    {
        $this->aplicaciones->removeElement($aplicaciones);
    }

    /**
     * Get aplicaciones
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAplicaciones()
    {
        return $this->aplicaciones;
    }
}