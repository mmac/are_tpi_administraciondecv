<?php
namespace tpare\DefaultBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Esta clase contiene los datos de los estudios que va a tener un curriculum
 * 
 * @ORM\Table(name="estudio")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class Estudio {
    
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=50)
     * @Assert\NotBlank(message = "Debe seleccionar un nivel")
     * 
     * Validation annotations...
     * @Assert\Choice(
     *  callback = "getNivelTypesCodes",
     *  message = "El tipo de nivel seleccionado no es válido."
     * )
     */
    protected $nivelEstudio;
    
    
    public static function getNivelTypes()
    {
        return array (
          'PRIMARIO'    => "Primario",
          'SECUNDARIO' => "Secundario",
          'TECNICO'=>"Tecnico",
          'PRE-GRADO' => "Pre Grado",
          'GRADO' => "Grado",
          'MAESTRIA'=>"Maestria",
          'DOCTORADO'=>"Doctorado"
        );
    }
    
    public static function getNivelTypesCodes()
    {
        return array_keys(self::getNivelTypes());
    }
    
    /**
     * @ORM\Column(type="string", length=50)
     * @Assert\NotBlank(message = "Debe ingresar un titulo para el estudio.")
     */
    protected $titulo; 
    
    /**
     * @ORM\Column(type="string", length=30)
     * @Assert\NotBlank(message = "Debe seleccionar un estado del estudio")
     * 
     * Validation annotations...
     * @Assert\Choice(
     *  callback = "getEstadoCodes",
     *  message = "El estado seleccionado no es válido."
     * )
     */
    protected $estadoEstudio; 
    
    public static function getEstadoTypes()
    {
        return array (
          'ENCURSO'    => "En curso",
          'FINALIZADO' => "Finalizado",
          'ABANDONADO'=>"Abandonado",
          
        );
    }
    
    public static function getEstadoCodes()
    {
        return array_keys(self::getEstadoTypes());
    }
    
    
    /**
     * @ORM\Column(type="string", length=50)
     * @Assert\NotBlank(message = "Debe ingresar una institución para el estudio.")
     */
    protected $institucion;
    
    /**
     * @ORM\Column(type="date", nullable=true, nullable=true)
     */
    protected $fechaInicio;
    
    /**
     * @ORM\Column(type="date", nullable=true, nullable=true)
     */
    protected $fechaFin;

    //falta validar que tenga solo 2 decimales y entre 0 y 10
    /**
    * @ORM\Column(type="decimal", scale=2, nullable=true)
     * @Assert\Range(
     *      min = 0,
     *      max=10,
     *      minMessage = "El promedio no puede ser menor a 0.",
     *      maxMessage = "El promedio no puede ser mayor a 10."
     * )
    */
    protected $promedio;
    
    /**
     *
     * @ORM\Column(type="integer", nullable=true))
     */
    protected $materiasTotales;//integer
    
    /**
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $materiasAprobadas;//integer    
    
    /**
     *
     * @ORM\Column(type="string", nullable=true )
     */
    protected $descripcion;//string
    
    /**
     *@ORM\ManyToOne(targetEntity="Curriculum", inversedBy="estudios")
     *@ORM\JoinColumn(name="curriculum_id", referencedColumnName="id")
     */
    protected $curriculum;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nivelEstudio
     *
     * @param string $nivelEstudio
     * @return estudio
     */
    public function setNivelEstudio($nivelEstudio)
    {
        $this->nivelEstudio = $nivelEstudio;
    
        return $this;
    }

    /**
     * Get nivelEstudio
     *
     * @return string 
     */
    public function getNivelEstudio()
    {
        return $this->nivelEstudio;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     * @return estudio
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;
    
        return $this;
    }

    /**
     * Get titulo
     *
     * @return string 
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set estadoEstudio
     *
     * @param string $estadoEstudio
     * @return estudio
     */
    public function setEstadoEstudio($estadoEstudio)
    {
        $this->estadoEstudio = $estadoEstudio;
    
        return $this;
    }

    /**
     * Get estadoEstudio
     *
     * @return string 
     */
    public function getEstadoEstudio()
    {
        return $this->estadoEstudio;
    }

    /**
     * Set institucion
     *
     * @param string $institucion
     * @return estudio
     */
    public function setInstitucion($institucion)
    {
        $this->institucion = $institucion;
    
        return $this;
    }

    /**
     * Get institucion
     *
     * @return string 
     */
    public function getInstitucion()
    {
        return $this->institucion;
    }

    /**
     * Set fechaInicio
     *
     * @param \DateTime $fechaInicio
     * @return estudio
     */
    public function setFechaInicio($fechaInicio)
    {
        $this->fechaInicio = $fechaInicio;
    
        return $this;
    }

    /**
     * Get fechaInicio
     *
     * @return \DateTime 
     */
    public function getFechaInicio()
    {
        return $this->fechaInicio;
    }

    /**
     * Set fechaFin
     *
     * @param \DateTime $fechaFin
     * @return estudio
     */
    public function setFechaFin($fechaFin)
    {
        $this->fechaFin = $fechaFin;
    
        return $this;
    }

    /**
     * Get fechaFin
     *
     * @return \DateTime 
     */
    public function getFechaFin()
    {
        return $this->fechaFin;
    }

    /**
     * Set promedio
     *
     * @param string $promedio
     * @return estudio
     */
    public function setPromedio( $promedio)
    {
        $this->promedio =  doubleval($promedio);
    
        return $this;
    }

    /**
     * Get promedio
     *
     * @return \double 
     */
    public function getPromedio()
    {
        return $this->promedio;
    }

    /**
     * Set materiasTotales
     *
     * @param integer $materiasTotales
     * @return estudio
     */
    public function setMateriasTotales($materiasTotales)
    {
        $this->materiasTotales = $materiasTotales;
    
        return $this;
    }

    /**
     * Get materiasTotales
     *
     * @return integer 
     */
    public function getMateriasTotales()
    {
        return $this->materiasTotales;
    }

    /**
     * Set materiasAprobadas
     *
     * @param integer $materiasAprobadas
     * @return estudio
     */
    public function setMateriasAprobadas($materiasAprobadas)
    {
        $this->materiasAprobadas = $materiasAprobadas;
    
        return $this;
    }

    /**
     * Get materiasAprobadas
     *
     * @return integer 
     */
    public function getMateriasAprobadas()
    {
        return $this->materiasAprobadas;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return estudio
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    
        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set curriculum
     *
     * @param \tpare\DefaultBundle\Entity\curriculum $curriculum
     * @return Estudio
     */
    public function setCurriculum(\tpare\DefaultBundle\Entity\curriculum $curriculum = null)
    {
        $this->curriculum = $curriculum;
    
        return $this;
    }

    /**
     * Get curriculum
     *
     * @return \tpare\DefaultBundle\Entity\curriculum 
     */
    public function getCurriculum()
    {
        return $this->curriculum;
    }
    
}