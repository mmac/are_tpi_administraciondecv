<?php
namespace tpare\DefaultBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 *
 * @ORM\Table(name="experienciaLaboral")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class ExperienciaLaboral {
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     *  @ORM\ManyToOne(targetEntity="Curriculum", inversedBy="experienciasLaborales")
     *  @ORM\JoinColumn(name="curriculum_id", referencedColumnName="id")
     */
    protected $curriculum;
    
    /**
     *@ORM\Column(type="string", length=50)
     *@Assert\NotBlank(message="Debe ingresar una empresa para la experiencia laboral")
     */
    protected $empresa;               
    
    /**
     *@ORM\Column(type="string", length=50, nullable=true)
     */
    protected $sector;                
    
    /**
     * @ORM\Column(type="string", length=50)
     * @Assert\NotBlank(message = "Debe seleccionar una jerarquia")
     * Validation annotations...
     * @Assert\Choice(
     *  callback = "getJerarquiaTypesCodes",
     *  message = "El tipo de jerarquía seleccionado no es válido."
     * )
     */
    protected $jerarquia;             
    
    public static function getJerarquiaTypes()
    {
        return array (
          'JUNIOR'    => "Junior",
          'SEMIJUNIOR' => "Semi Senior",
          'SENIOR' => "Senior",
        );
    }
    
    public static function getJerarquiaTypesCodes()
    {
        return array_keys(self::getJerarquiaTypes());
    }
    
    /**
    * @ORM\ManyToOne(targetEntity="Rol")
    * @ORM\JoinColumn(name="cargo_id", referencedColumnName="id")
    * @Assert\NotBlank(message = "Debe seleccionar un cargo.")
    */
    protected $cargo;                
    
    /**
     *@ORM\Column(type="integer", nullable=true)
     */
    protected $personasACargo;       
    
    /**
     *@ORM\Column(type="date")
     * @Assert\NotBlank(message="Debe ingresar una fecha de inicio.")
     */
    protected $fechaInicio;          
    
    /**
     *@ORM\Column(type="date", nullable=true)
     * 
     */
    protected $fechaFin;             
    
    /**
     *@ORM\Column(type="string", length=300, nullable=true)
     */
    protected $motivoFinalizacion;   
    
    /**
     *@ORM\Column(type="string", length=300, nullable=true)
     */
    protected $descripcionFuncion;   
   
    
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set curriculum
     *
     * @param \tpare\DefaultBundle\Entity\curriculum $curriculum
     * @return ExperienciaLaboral
     */
    public function setCurriculum(\tpare\DefaultBundle\Entity\curriculum $curriculum = null)
    {
        $this->curriculum = $curriculum;
    
        return $this;
    }

    /**
     * Get curriculum
     *
     * @return \tpare\DefaultBundle\Entity\curriculum 
     */
    public function getCurriculum()
    {
        return $this->curriculum;
    }

    /**
     * Set empresa
     *
     * @param string $empresa
     * @return ExperienciaLaboral
     */
    public function setEmpresa($empresa)
    {
        $this->empresa = $empresa;
    
        return $this;
    }

    /**
     * Get empresa
     *
     * @return string 
     */
    public function getEmpresa()
    {
        return $this->empresa;
    }

    /**
     * Set sector
     *
     * @param string $sector
     * @return ExperienciaLaboral
     */
    public function setSector($sector)
    {
        $this->sector = $sector;
    
        return $this;
    }

    /**
     * Get sector
     *
     * @return string 
     */
    public function getSector()
    {
        return $this->sector;
    }

    /**
     * Set jerarquia
     *
     * @param string $jerarquia
     * @return ExperienciaLaboral
     */
    public function setJerarquia($jerarquia)
    {
        $this->jerarquia = $jerarquia;
    
        return $this;
    }

    /**
     * Get jerarquia
     *
     * @return string 
     */
    public function getJerarquia()
    {
        return $this->jerarquia;
    }

    /**
     * Set cargo
     *
     * @param string $cargo
     * @return ExperienciaLaboral
     */
    public function setCargo($cargo)
    {
        $this->cargo = $cargo;
    
        return $this;
    }

    /**
     * Get cargo
     *
     * @return string 
     */
    public function getCargo()
    {
        return $this->cargo;
    }

    /**
     * Set personasACargo
     *
     * @param integer $personasACargo
     * @return ExperienciaLaboral
     */
    public function setPersonasACargo($personasACargo)
    {
        $this->personasACargo = $personasACargo;
    
        return $this;
    }

    /**
     * Get personasACargo
     *
     * @return integer 
     */
    public function getPersonasACargo()
    {
        return $this->personasACargo;
    }

    /**
     * Set fechaInicio
     *
     * @param \DateTime $fechaInicio
     * @return ExperienciaLaboral
     */
    public function setFechaInicio($fechaInicio)
    {
        $this->fechaInicio = $fechaInicio;
    
        return $this;
    }

    /**
     * Get fechaInicio
     *
     * @return \DateTime 
     */
    public function getFechaInicio()
    {
        return $this->fechaInicio;
    }

    /**
     * Set fechaFin
     *
     * @param \DateTime $fechaFin
     * @return ExperienciaLaboral
     */
    public function setFechaFin($fechaFin)
    {
        $this->fechaFin = $fechaFin;
    
        return $this;
    }

    /**
     * Get fechaFin
     *
     * @return \DateTime 
     */
    public function getFechaFin()
    {
        return $this->fechaFin;
    }

    /**
     * Set motivoFinalizacion
     *
     * @param string $motivoFinalizacion
     * @return ExperienciaLaboral
     */
    public function setMotivoFinalizacion($motivoFinalizacion)
    {
        $this->motivoFinalizacion = $motivoFinalizacion;
    
        return $this;
    }

    /**
     * Get motivoFinalizacion
     *
     * @return string 
     */
    public function getMotivoFinalizacion()
    {
        return $this->motivoFinalizacion;
    }

    /**
     * Set descripcionFuncion
     *
     * @param string $descripcionFuncion
     * @return ExperienciaLaboral
     */
    public function setDescripcionFuncion($descripcionFuncion)
    {
        $this->descripcionFuncion = $descripcionFuncion;
    
        return $this;
    }

    /**
     * Get descripcionFuncion
     *
     * @return string 
     */
    public function getDescripcionFuncion()
    {
        return $this->descripcionFuncion;
    }
    
    

}
