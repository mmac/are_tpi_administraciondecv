<?php
namespace tpare\DefaultBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use tpare\DefaultBundle\Entity\Curriculum;
use tpare\DefaultBundle\Forms\IdiomaPostulanteType;
use tpare\DefaultBundle\Entity\IdiomaPostulante;
use tpare\DefaultBundle\Entity\CertificadoIdioma;
/**
 * @author martinmac
 */
class IdiomaPostulanteController extends Controller{
    
     public function nuevoIdiomaAction(Request $request, $idIdioma){
        
         $user=  $this->getUser();
         $curriculum=$user->getCurriculum();
         
         /*
         * Lo busca en la BD, si no lo encuentra es NULL y el form va a estar vacio.
         */
        $idioma = $this->getDoctrine()->getRepository('DefaultBundle:IdiomaPostulante')->find($idIdioma);            

                  
        $form = $this->createForm(new IdiomaPostulanteType(), $idioma);
        
        /*
         * Si el método es post, le hago un bind al formulario con los datos del request
         * y luego valido los datos. Si no entra al if, simplemente se renderiza el formulario vacio
         */
        if ($request->isMethod('POST')) {
            
            $form->bind($request);

            if ($form->isValid()) {
                
                $idioma = $form->getData();
                
                $curriculum->addIdioma($idioma);
                $idioma->setCurriculum($curriculum);

              
                
                //se tira el objeto a la BD.
                $em = $this->getDoctrine()->getManager();
                $em->persist($idioma);               
                $em->persist($curriculum);
                $em->flush();
                
                 $this->get('session')->getFlashBag()->add(
                    'exito',
                    'Has registrado con éxito el idioma.'
                     );
                 
                return $this->redirect($this->generateUrl('default_show_curriculum', 
                        array('idCurriculum'=>$curriculum->getId())));
            }
            
            $this->get('session')->getFlashBag()->add(
                    'error',
                    'Hubo un error dentro del formulario. Por favor verifica los datos ingresados.'
                     );
         
        }
     
        return $this->render('DefaultBundle:Formularios:nuevoIdiomaPostulante.html.twig', array('form'=>$form->createView(),'idIdioma'=>$idIdioma));
     
    }
    
    public function eliminarAction($idIdioma){
        $user = $this->getUser(); 
        $curriculum = $user->getCurriculum();
         
        if($user == null){
            $this->get('session')->getFlashBag()->add(
                    'error',
                    'Por favor inicie sesion antes eliminar un idioma.'
                     );
            
            return $this->redirect($this->generateUrl('default_home'));
        }
        
        $idioma = $this->getDoctrine()->getRepository('DefaultBundle:IdiomaPostulante')->find($idIdioma);
        
        if($idioma != null){
            $em = $this->getDoctrine()->getManager();
            $em->remove($idioma);
            $em->flush();
                
            $this->get('session')->getFlashBag()->add(
                'exito',
                'El idioma se ha eliminado con éxito.'
            );
            
            return $this->redirect($this->generateUrl('default_show_curriculum', 
                   array('idCurriculum'=>$curriculum->getId())));
        }else{
            $this->get('session')->getFlashBag()->add(
                    'error',
                    'Ese idioma no existe.'
                     );
            
            return $this->redirect($this->generateUrl('default_home'));           
        }
        
        return $this->redirect($this->generateUrl('default_panel_empresa'));
        
     }
     
     public function updateAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $idioma = $em->getRepository('DefaultBundle:IdiomaPostulante')->find($id);

        if (!$idioma) {
            throw $this->createNotFoundException('Idioma no se encuentra '.$id);
        }

        $certificadosOriginales = array();

        // Create an array of the current Tag objects in the database
        foreach ($idioma->getCertificados() as $certificado) {
            $certificadosOriginales[] = $certificado;
        }

        $updateForm = $this->createForm(new IdiomaPostulanteType(), $idioma);

        
        
        $updateForm->handleRequest($request);

        if ($updateForm->isValid()) {

            // filter $originalTags to contain tags no longer present
            foreach ($idioma->getCertificados() as $certificado) {
                foreach ($certificadosOriginales as $key => $toDel) {
                    if ($toDel->getId() === $certificado->getId()) {
                        unset($certificadosOriginales[$key]);
                    }
                }
            }
            
            $em = $this->getDoctrine()->getManager();
            
            
            
            // remove the relationship between the tag and the Task
            foreach ($certificadosOriginales as $certificado) {
                
                
                // remove the Task from the Tag
            //$tag->getTasks()->removeElement($task);
            
                $certificado->getIdioma()->removeCertificado($idioma);
                
                 //$em->persist($tag);
                $em->persist($certificado);
            }

            $em->persist($idioma);
            $em->flush();

             $this->get('session')->getFlashBag()->add(
                'exito',
                'El idioma se ha eliminado con éxito.'
            );
            
            return $this->redirect($this->generateUrl('default_show_curriculum', 
                   array('idCurriculum'=>$curriculum->getId())));
        }

        $this->get('session')->getFlashBag()->add(
                    'error',
                    'Ese idioma no existe.'
                     );
            
            return $this->redirect($this->generateUrl('default_home')); 
    }
}

?>
