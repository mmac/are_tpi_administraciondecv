<?php
namespace tpare\DefaultBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use tpare\DefaultBundle\Entity\OfertaLaboral;
use tpare\DefaultBundle\Entity\Requerimiento;
use tpare\DefaultBundle\Forms\OfertaLaboralType;
use tpare\DefaultBundle\Entity\Filter\OfertaFilter;
use tpare\DefaultBundle\Forms\Filter\OfertaFilterType;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Description of OfertaLaboralController
 *
 * @author ale
 */
class OfertaLaboralController extends Controller{
    
    public function nuevaOfertaLaboralAction(Request $request){
        
        $ofertaLaboral = new OfertaLaboral();
//        $ofertaLaboral->addRequerimiento(new Requerimiento());
        
        $form = $this->createForm(new OfertaLaboralType(), $ofertaLaboral);
       
        /*
         * Si el método es post, le hago un bind al formulario con los datos del request
         * y luego valido los datos. Si no entra al if, simplemente se renderiza el formulario vacio
         */
        if ($request->isMethod('POST')) {
            
            $form->bind($request);

            if ($form->isValid()) {
                
                $ofertaLaboral = $form->getData();    
                $user = $this->getUser();
                
                $ofertaLaboral->setUser($user);

                //se tira el objeto a la BD.
                $em = $this->getDoctrine()->getManager();
                $em->persist($ofertaLaboral);
                $em->flush();
                
                 $this->get('session')->getFlashBag()->add(
                    'exito',
                    'Has registrado con éxito la oferta laboral. 
                     Cuando la fecha de inicio se cumpla, los usuarios la podrán verla para postularse.'
                     );

                return $this->redirect($this->generateUrl('default_home'));
            }
            
            $this->get('session')->getFlashBag()->add(
                    'error',
                    'Hubo un error dentro del formulario. Por favor verifica los datos ingresados.'
                     );
         
        }
              
        return $this->render('DefaultBundle:Formularios:nuevaOfertaLaboral.html.twig', array('form'=>$form->createView()));
     }
     
     public function showOfertaLaboralAction($idOferta){
         $oferta = $this->getDoctrine()->getRepository('DefaultBundle:OfertaLaboral')->find($idOferta);
        //$ofertasLaborales = $this->getDoctrine()->getRepository('DefaultBundle:OfertaLaboral')->findBy(array('between'=>), $orderBy)
        
        return $this->render('DefaultBundle::showOferta.html.twig', array('oferta'=>$oferta));     
     }
     
     public function filtrarOfertasAction(Request $request){
         
         $data = $request->request->all();
         
         if(array_key_exists( 'tecnologia' , $data['OfertaLaboralFilterForm'])){             
            $tecnologiaFilters = $data['OfertaLaboralFilterForm']['tecnologia'];
         }else{
            $tecnologiaFilters = null;         
         }
         
         if(array_key_exists( 'rol' , $data['OfertaLaboralFilterForm'])){             
            $rolFilters = $data['OfertaLaboralFilterForm']['rol'];
         }else{
            $rolFilters = null;         
         }
         
         if(array_key_exists( 'jerarquia' , $data['OfertaLaboralFilterForm'])){             
            $jerarquiaFilters = $data['OfertaLaboralFilterForm']['jerarquia'];
         }else{
            $jerarquiaFilters = null;         
         }
         
         $ofertas = $this->getDoctrine()->getRepository('DefaultBundle:OfertaLaboral')->filtrarOfertas($tecnologiaFilters, $rolFilters, $jerarquiaFilters);
         if(count($ofertas) == 0){
             $ofertas = null;
         }
         
         $ofertaFilter = new OfertaFilter();     
         $form = $this->createForm(new OfertaFilterType(), $ofertaFilter);
        
         return $this->render('DefaultBundle::home.html.twig', array('roles'=>$rolFilters ,'ofertasLaborales'=>$ofertas, 'form'=>$form->createView()));
     }
     
     public function updateAction(Request $request, $idOferta){
        $user = $this->getUser(); 
         
        if($user == null){
            $this->get('session')->getFlashBag()->add(
                    'error',
                    'Por favor inicie sesion antes de modificar los datos.'
                     );
            
            $this->redirect($this->generateUrl('default_home'));
        }

        $ofertaLaboral = $this->getDoctrine()->getRepository('DefaultBundle:OfertaLaboral')->find($idOferta);
        
        $form = $this->createForm(new OfertaLaboralType(), $ofertaLaboral);
       
        /*
         * Si el método es post, le hago un bind al formulario con los datos del request
         * y luego valido los datos. Si no entra al if, simplemente se renderiza el formulario vacio
         */
        if ($request->isMethod('POST')) {
            
            $form->bind($request);

            if ($form->isValid()) {
                
                $ofertaLaboral = $form->getData();    
                $user = $this->getUser();
                
                $ofertaLaboral->setUser($user);

                //se tira el objeto a la BD.
                $em = $this->getDoctrine()->getManager();
                $em->persist($ofertaLaboral);
                $em->flush();
                
                 $this->get('session')->getFlashBag()->add(
                    'exito',
                    'Has modificado con éxito la oferta laboral. 
                     Cuando la fecha de inicio se cumpla, los usuarios la podrán verla para postularse.'
                     );

                return $this->redirect($this->generateUrl('default_panel_empresa'));
            }
            
            $this->get('session')->getFlashBag()->add(
                    'error',
                    'Hubo un error dentro del formulario. Por favor verifica los datos ingresados.'
                     );
         
        }
              
        return $this->render('DefaultBundle:Formularios:updateOfertaLaboral.html.twig', 
                array('form'=>$form->createView(), 'idOferta'=>$idOferta));
     }
     
     public function eliminarAction($idOferta){
        $user = $this->getUser(); 
         
        if($user == null){
            $this->get('session')->getFlashBag()->add(
                    'error',
                    'Por favor inicie sesion antes eliminar una oferta laboral.'
                     );
            
            return $this->redirect($this->generateUrl('default_home'));
        }
        
        $ofertaLaboral = $this->getDoctrine()->getRepository('DefaultBundle:OfertaLaboral')->find($idOferta);
        
        if($ofertaLaboral != null){
            $em = $this->getDoctrine()->getManager();
            $em->remove($ofertaLaboral);
            $em->flush();
                
            $this->get('session')->getFlashBag()->add(
                'exito',
                'La oferta laboral se ha eliminado con éxito.'
            );
                
            return $this->redirect($this->generateUrl('default_ofertas_subidas'));
        }else{
            $this->get('session')->getFlashBag()->add(
                    'error',
                    'Esa oferta laboral no existe.'
                     );
            
           return $this->redirect($this->generateUrl('default_home'));
        }
        
        return $this->redirect($this->generateUrl('default_panel_empresa'));
        
     }
}

?>
