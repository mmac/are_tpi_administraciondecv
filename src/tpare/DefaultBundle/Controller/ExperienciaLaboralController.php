<?php
namespace tpare\DefaultBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use tpare\DefaultBundle\Entity\Curriculum;
use tpare\DefaultBundle\Forms\ExperienciaLaboralType;
use tpare\DefaultBundle\Entity\ExperienciaLaboral;
/**
 * Description of ExperienciaLaboralController
 *
 * @author martinmac
 */
class ExperienciaLaboralController extends Controller {
    
    public function nuevaExperienciaLaboralAction(Request $request, $idExpLab){
        
         $user = $this->getUser();
         
        $curriculum=$user->getCurriculum();
        
         /*
         * Lo busca en la BD, si no lo encuentra es NULL y el form va a estar vacio.
         */
        $expLaboral = $this->getDoctrine()->getRepository('DefaultBundle:ExperienciaLaboral')->find($idExpLab);            

        ;
        
        $form = $this->createForm(new ExperienciaLaboralType(), $expLaboral);
        
        /*
         * Si el método es post, le hago un bind al formulario con los datos del request
         * y luego valido los datos. Si no entra al if, simplemente se renderiza el formulario vacio
         */
        if ($request->isMethod('POST')) {
            
            $form->bind($request);

            if ($form->isValid()) {
                
                $expLaboral = $form->getData();                

                $curriculum->addExperienciasLaborale($expLaboral);
                $expLaboral->setCurriculum($curriculum);
                
                //se tira el objeto a la BD.
                $em = $this->getDoctrine()->getManager();
                $em->persist($expLaboral);                
                $em->persist($curriculum);
                $em->flush();
                
                 $this->get('session')->getFlashBag()->add(
                    'exito',
                    'Has registrado con éxito la experiencia laboral.'
                     );
                 
                return $this->redirect($this->generateUrl('default_show_curriculum', 
                        array('idCurriculum'=>$curriculum->getId())));
            }
            
            $this->get('session')->getFlashBag()->add(
                    'error',
                    'Hubo un error dentro del formulario. Por favor verifica los datos ingresados.'
                     );
         
        }
     
        return $this->render('DefaultBundle:Formularios:nuevaExperienciaLaboral.html.twig', array('form'=>$form->createView(),'idExpLab'=>$idExpLab));
     
    }
    
    public function eliminarAction($idExpLab){
        $user = $this->getUser(); 
        $curriculum = $user->getCurriculum();
         
        if($user == null){
            $this->get('session')->getFlashBag()->add(
                    'error',
                    'Por favor inicie sesion antes eliminar una experiencia laboral.'
                     );
            
            return $this->redirect($this->generateUrl('default_home'));
        }
        
        $experienciaLaboral = $this->getDoctrine()->getRepository('DefaultBundle:ExperienciaLaboral')->find($idExpLab);
        
        if($experienciaLaboral != null){
            $em = $this->getDoctrine()->getManager();
            $em->remove($experienciaLaboral);
            $em->flush();
                
            $this->get('session')->getFlashBag()->add(
                'exito',
                'La experiencia laboral se ha eliminado con éxito.'
            );
            
            return $this->redirect($this->generateUrl('default_show_curriculum', 
                   array('idCurriculum'=>$curriculum->getId())));
        }else{
            $this->get('session')->getFlashBag()->add(
                    'error',
                    'Esa experiencia laboral no existe.'
                     );
            
            return $this->redirect($this->generateUrl('default_home'));           
        }
        
        return $this->redirect($this->generateUrl('default_panel_empresa'));
        
     }
}

?>
