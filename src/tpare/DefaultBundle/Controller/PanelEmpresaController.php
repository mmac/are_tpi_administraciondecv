<?php

namespace tpare\DefaultBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use tpare\DefaultBundle\Entity\Filter\OfertaFilter;
use tpare\DefaultBundle\Forms\Filter\OfertaFilterType;

/**
 * Description of PanelEmpresaController
 *
 * @author ale
 */
class PanelEmpresaController extends Controller{
    
    public function indexAction(){
        $user = $this->getUser();
        
        return $this->render('DefaultBundle::panelEmpresa.html.twig', array('user'=>$user));
    }
    
    public function ofertasSubidasAction(){
        $user = $this->getUser();
        
        $ofertas = $this->getDoctrine()->getRepository('DefaultBundle:OfertaLaboral')->findBy(array('user'=>$user));
        
        if(count($ofertas) == 0){
            $ofertas = null;
        }
        
        return $this->render('DefaultBundle::panelEmpresa.html.twig', array('ofertasSubidas'=>$ofertas));
    }
    
    public function aplicacionesUsuarioAction(){
        $user = $this->getUser();
        
        $aplicaciones = $user->getAplicaciones();
        
        if(count($aplicaciones) == 0){
            $aplicaciones = null;
        }
        
        return $this->render('DefaultBundle::panelEmpresa.html.twig', 
                array('aplicaciones'=>$aplicaciones));        
    }
}

?>
