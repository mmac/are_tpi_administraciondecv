<?php
namespace tpare\DefaultBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use tpare\DefaultBundle\Entity\Curriculum;
use tpare\DefaultBundle\Forms\CursoType;
use tpare\DefaultBundle\Entity\Estudio;

/**
 * Description of CursoController
 *
 * @author ale
 */
class CursoController extends Controller{
    
    public function nuevoCursoAction(Request $request, $idCurso){
        
        $user = $this->getUser();
        $curriculum = $user->getCurriculum();
        
        /*
         * Lo busca en la BD, si no lo encuentra es NULL y el form va a estar vacio.
         */
        $curso = $this->getDoctrine()->getRepository('DefaultBundle:Curso')->find($idCurso);            
            
        $form = $this->createForm(new CursoType(), $curso);
        
        /*
         * Si el método es post, le hago un bind al formulario con los datos del request
         * y luego valido los datos. Si no entra al if, simplemente se renderiza el formulario vacio
         */
        if ($request->isMethod('POST')) {
            
            $form->bind($request);

            if ($form->isValid()) {
                
                $curso = $form->getData();                
                
                $curriculum->addCurso($curso);
                $curso->setCurriculum($curriculum);
                
                //se tira el objeto a la BD.
                $em = $this->getDoctrine()->getManager();
                $em->persist($curso);                 
                $em->persist($curriculum);
                $em->flush();
                
                 $this->get('session')->getFlashBag()->add(
                    'exito',
                    'Has registrado con éxito el curso.'
                     );
                 
                return $this->redirect($this->generateUrl('default_show_curriculum', 
                        array('idCurriculum'=>$curriculum->getId())));
            }
            
            $this->get('session')->getFlashBag()->add(
                    'error',
                    'Hubo un error dentro del formulario. Por favor verifica los datos ingresados.'
                     );       
        }
     
        return $this->render('DefaultBundle:Formularios:nuevoCurso.html.twig', array('form'=>$form->createView(), 'idCurso'=>$idCurso));     
    }
    
    public function eliminarAction($idCurso){
        
        $user = $this->getUser(); 
        $curriculum = $user->getCurriculum();
         
        if($user == null){
            $this->get('session')->getFlashBag()->add(
                    'error',
                    'Por favor inicie sesion antes eliminar un estudio.'
                     );
            
            return $this->redirect($this->generateUrl('default_home'));
        }
        
        $curso = $this->getDoctrine()->getRepository('DefaultBundle:Estudio')->find($idCurso);
        
        if($curso != null){
            $em = $this->getDoctrine()->getManager();
            $em->remove($curso);
            $em->flush();
                
            $this->get('session')->getFlashBag()->add(
                'exito',
                'El curso se ha eliminado con éxito.'
            );
            
            return $this->redirect($this->generateUrl('default_show_curriculum', 
                   array('idCurriculum'=>$curriculum->getId())));
        }else{
            $this->get('session')->getFlashBag()->add(
                    'error',
                    'Ese curso no existe.'
                     );
            
            return $this->redirect($this->generateUrl('default_home'));           
        }
        
        return $this->redirect($this->generateUrl('default_panel_empresa'));
    }
}

?>
