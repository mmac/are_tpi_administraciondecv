<?php
namespace tpare\DefaultBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use tpare\DefaultBundle\Entity\InformacionExtra;
use tpare\DefaultBundle\Forms\InformacionExtraType;


class InformacionExtraController extends Controller{
    
    public function nuevaInformacioExtraAction(Request $request){
        
        $data = $request->request->all();
        
        $descripcion=$data['descripcion'];
        
        $user=  $this->getUser();
        $curriculum = $user->getCurriculum();
        $informacionExtra = new InformacionExtra();
       
        $informacionExtra->setDescripcion($descripcion);
                                      

        $curriculum->addInformacioExtra($informacionExtra);
        $informacionExtra->setCurriculum($curriculum);

        //se tira el objeto a la BD.
        $em = $this->getDoctrine()->getManager();
        $em->persist($informacionExtra);                 
        $em->persist($curriculum);
        $em->flush();

         $this->get('session')->getFlashBag()->add(
            'exito',
            'Has registrado con éxito la informacion adicional.'
             );

        return $this->redirect($this->generateUrl('default_show_curriculum', 
                array('idCurriculum'=>$curriculum->getId())));


        $this->get('session')->getFlashBag()->add(
                'error',
                'Hubo un error dentro del formulario. Por favor verifica los datos ingresados.'
                 );       
        
     
        return $this->render('DefaultBundle:Formularios:nuevoConocimiento.html.twig', 
                 array('form'=>$form->createView(),));    
    }
    
    public function eliminarAction($idInformacionExtra){
        
        $user = $this->getUser(); 
        $curriculum = $user->getCurriculum();
         
        if($user == null){
            $this->get('session')->getFlashBag()->add(
                    'error',
                    'Por favor inicie sesion antes eliminar informacion.'
                     );
            
            return $this->redirect($this->generateUrl('default_home'));
        }
        
        $informacionExtra = $this->getDoctrine()->getRepository('DefaultBundle:InformacionExtra')->find($idInformacionExtra);
        
        if($informacionExtra != null){
            $em = $this->getDoctrine()->getManager();
            $em->remove($informacionExtra);
            $em->flush();
                
            $this->get('session')->getFlashBag()->add(
                'exito',
                'La información se ha eliminado con éxito.'
            );
            
            return $this->redirect($this->generateUrl('default_show_curriculum', 
                   array('idCurriculum'=>$curriculum->getId())));
        }else{
            $this->get('session')->getFlashBag()->add(
                    'error',
                    'Esa información no existe.'
                     );
            
            return $this->redirect($this->generateUrl('default_home'));           
        }
        
        return $this->redirect($this->generateUrl('default_panel_empresa'));
        
     }
}

?>
