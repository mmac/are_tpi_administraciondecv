<?php

namespace tpare\DefaultBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use tpare\DefaultBundle\Entity\Curriculum;
use tpare\DefaultBundle\Forms\CurriculumType;
use tpare\DefaultBundle\Forms\Filter\CVFilterType;
use tpare\DefaultBundle\Entity\Estudio;
use tpare\DefaultBundle\Entity\Conocimiento;
use tpare\DefaultBundle\Entity\Requerimiento;
use tpare\DefaultBundle\Entity\DatosPersonales;
use tpare\DefaultBundle\Entity\ExperienciaLaboral;
use tpare\DefaultBundle\Entity\IdiomaPostulante;
use tpare\DefaultBundle\Forms\Filter\ListadoCurriculumFilterType;
use Doctrine\Common\Collections\ArrayCollection;
use tpare\DefaultBundle\Entity\Filter\CurriculumFilter;

class CurriculumController extends Controller{
    
    public function nuevoCurriculumAction(Request $request){
        $curriculum= new Curriculum();
        $curriculum->addEstudio(new Estudio());
        $personal= new DatosPersonales();
        $curriculum->addConocimiento(new Conocimiento());
        $curriculum->addIdioma(new IdiomaPostulante());
        $curriculum->addExperienciasLaborale(new ExperienciaLaboral());
        
        $form = $this->createForm(new CurriculumType(), $curriculum);
        
        /*
         * Si el método es post, le hago un bind al formulario con los datos del request
         * y luego valido los datos. Si no entra al if, simplemente se renderiza el formulario vacio
         */
        if ($request->isMethod('POST')) {
            
            $form->bind($request);

            if ($form->isValid()) {
                
                $curriculum = $form->getData();                

                //se tira el objeto a la BD.
                $em = $this->getDoctrine()->getManager();
                $em->persist($curriculum);
                $em->flush();
                
                 $this->get('session')->getFlashBag()->add(
                    'exito',
                    'Has registrado con éxito el curriculum.'
                     );

                return $this->redirect($this->generateUrl('default_home'));
            }
            
            $this->get('session')->getFlashBag()->add(
                    'error',
                    'Hubo un error dentro del formulario. Por favor verifica los datos ingresados.'
                     );
         
        }
     
        return $this->render('DefaultBundle:Formularios:nuevoCurriculum.html.twig', array('form'=>$form->createView()));
     
    }
    
    public function showCurriculumAction($idCurriculum){
        $curriculum = $this->getDoctrine()->getRepository('DefaultBundle:Curriculum')->find($idCurriculum);
        
        return $this->render('DefaultBundle::panelEmpresa.html.twig', array('curriculum'=>$curriculum));        
    }   
    
    public function listarCurriculumsAction($idOferta){
        
        $curriculums = new ArrayCollection();  
        $cvFilter = new \tpare\DefaultBundle\Entity\Filter\CVFilter;
        $requerimientos = $this->getDoctrine()->getRepository('DefaultBundle:Requerimiento')->findByOferta($idOferta);
        $cvFilter->setRequerimientos($requerimientos);
        $form = $this->createForm(new CVFilterType(), $cvFilter);
                
        $ofertaLaboral = $this->getDoctrine()->getRepository('DefaultBundle:OfertaLaboral')->find($idOferta);        
        
        $aplicaciones = $this->getDoctrine()->getRepository('DefaultBundle:Aplicacion')->findByOfertaLaboral($idOferta);        
  
        foreach ($aplicaciones as $aplicacion) {
            $cv = $aplicacion->getUser()->getCurriculum();
            $cv->addPuntaje(0);
            $curriculums->add($cv);
        }
        
        $filtrarNoCumplenRequisitos = true;              
               
        $this->puntuarExperienciaLaboral($curriculums);
        $this->puntuarEstudios($curriculums);
        $this->puntuarIdioma($curriculums, $ofertaLaboral);
        $this->puntuarRequerimientos($curriculums, $requerimientos, $filtrarNoCumplenRequisitos);
                
        $this->ordenarCurriculums($curriculums);
        
        $this->filtrarNoCumplenReq($curriculums);
        
        return $this->render('DefaultBundle::listadoCurriculum.html.twig', 
                array('curriculums'=>$curriculums, 'form'=>$form->createView(), 'oferta'=>$ofertaLaboral));        
    }
    
    public function filtrarCurriculumsAction($idOferta, Request $request){
        $curriculums = new ArrayCollection();  
        $filtros = new ArrayCollection();  
        
        $cvFilter = new \tpare\DefaultBundle\Entity\Filter\CVFilter;
        $form = $this->createForm(new CVFilterType(), $cvFilter);
        $form->handleRequest($request);
        
        $ofertaLaboral = $this->getDoctrine()->getRepository('DefaultBundle:OfertaLaboral')->find($idOferta);        
        $aplicaciones = $this->getDoctrine()->getRepository('DefaultBundle:Aplicacion')->findByOfertaLaboral($idOferta);        
              
        foreach ($aplicaciones as $aplicacion) {
            $cv = $aplicacion->getUser()->getCurriculum();
            $cv->addPuntaje(0);
            $curriculums->add($cv);
        }
        
        $data = $request->request->all();
        
        $filtrarNoCumplenRequisitos = true;
                 
        //XXX: Esto sólo sirve para arreglar las negradas posteriores
        if ($data == null) {
            $data = array();
            $data['CVFilterForm'] = array();
        }
        
        $this->puntuarRequerimientos($curriculums, $cvFilter->getRequerimientos(), $filtrarNoCumplenRequisitos);
        $this->puntuarExperienciaLaboral($curriculums);
        $this->puntuarEstudios($curriculums);
        $this->puntuarIdioma($curriculums, $ofertaLaboral);

        $this->ordenarCurriculums($curriculums);
        
        if(array_key_exists( 'listarCVReqNoCumplidos' , $data['CVFilterForm']) &&
                            $data['CVFilterForm']['listarCVReqNoCumplidos'] == 'si'){
            $filtros->add('CVs sin requerimientos cumplidos');
        }else{
            $this->filtrarNoCumplenReq($curriculums);            
        }
                 
        if(array_key_exists( 'tituloExcluyente' , $data['CVFilterForm']) && $data['CVFilterForm']['tituloExcluyente'] == 'si'){             
           $this->filtrarPorTitulo($curriculums);
           $filtros->add('Titulo de grado excluyente');
        }
        
        if(array_key_exists( 'idiomaExcluyente' , $data['CVFilterForm']) && $data['CVFilterForm']['idiomaExcluyente'] == 'si'){             
           $this->filtrarPorIdioma($curriculums, $ofertaLaboral);
           $filtros->add('Idioma excluyente');           
        }
        
        if(array_key_exists( 'tecnologia' , $data['CVFilterForm'])){
           $tecnologias =  $data['CVFilterForm']['tecnologia'];
           $this->filtrarPorTecnologia($curriculums, $tecnologias);
           $filtros->add('Tecnologías');
        }
        
        if(array_key_exists( 'sexo' , $data['CVFilterForm'])){
           $sexosPreferentes =  $data['CVFilterForm']['sexo'];
           $this->filtrarPorSexo($curriculums, $sexosPreferentes);
           foreach ($sexosPreferentes as $tec) {
               $filtros->add($tec);
           }
        }
        
        if(array_key_exists( 'cargo' , $data['CVFilterForm'])){
           $idCargo =  $data['CVFilterForm']['cargo'];
           if(!array_key_exists( 'anos' , $data['CVFilterForm'])){
               $añosMin = 0;
           }else{
               $añosMin =  $data['CVFilterForm']['anos'];
           }
           $this->filtrarPorExperiencia($curriculums, $idCargo, $añosMin);
           $filtros->add("Experiencia laboral previa");
        }
        
        
        return $this->render('DefaultBundle::listadoCurriculum.html.twig', 
                array('curriculums'=>$curriculums, 'form'=>$form->createView(), 'oferta'=>$ofertaLaboral, 'filtros'=>$filtros)); 
        
    }
    
    private function ordenarCurriculums(ArrayCollection $curriculums){
        
        for ($i = 0; $i < count($curriculums); $i++) {
            for($j = $i + 1; $j < count($curriculums); $j++){     
                if($curriculums->get($i)->getPuntaje() < $curriculums->get($j)->getPuntaje()){
                    $aux = $curriculums->get($j);
                    $curriculums->set($j, $curriculums->get($i));
                    $curriculums->set($i, $aux);
                }
            }
        }               
    }


    private function filtrarPorExperiencia($curriculums, $idCargo, $añosMin){
        $pasa = false;
        
        foreach ($curriculums as $cv) {
            $pasa = false;
            
            foreach ($cv->getExperienciasLaborales() as $exp) {    
                $pasa = false; 
                if($exp->getCargo()->getId() == $idCargo){
                    
                    $fechaInicio = $exp->getFechaInicio();
                    $fechaFin = $exp->getFechaFin();
                    $diff = abs(strtotime($fechaFin->format('d-m-Y')) - strtotime($fechaInicio->format('d-m-Y')));
                    $añosExp = round(($diff/(365*24*60*60)));
                    
                    if($añosExp >= $añosMin){
                        $pasa = true; 
                        break;
                    }
                }
            } 
            if(!$pasa){
                $curriculums->removeElement($cv);                    
                $pasa = false;
            }                                           
            
            if($cv->getExperienciasLaborales() == null){
                $curriculums->removeElement($cv);
            }
            
            
        }
    }
    
     private function filtrarPorSexo($curriculums, $sexosPreferentes){
        $pasa = false;
        
        foreach ($curriculums as $cv) {
            $pasa = false;
            
            foreach ($sexosPreferentes as $sexo) {                 
                if($cv->getDatosPersonales()->getSexo() == $sexo){
                    $pasa = true;
                    break;
                }

            }              
            if(!$pasa){
                $curriculums->removeElement($cv);             
                $pasa = false;
            }                                           
        }
    }
    
    private function filtrarPorTecnologia($curriculums, $tecnologias){
        $pasa = false;
        
        foreach ($curriculums as $cv) {
            $pasa = false;
            
            foreach ($tecnologias as $idTecnologia) {
                $pasa = false; 
                
                foreach ($cv->getConocimientos() as $conocimiento){
                    
                    if($conocimiento->getTecnologia()->getId() == $idTecnologia){
                        $pasa = true;
                        break;
                    }

                }
                
                if(!$pasa){
                    $curriculums->removeElement($cv);             
                    $pasa = false;
                    break;
                }                           
            }              
        }
    }

    private function filtrarPorIdioma(ArrayCollection $curriculums, $oferta){
        $pasa = false;
//        $cvsFiltrados = new ArrayCollection();
        
        foreach ($curriculums as $cv) {
            $pasa = false;
            
            foreach ($cv->getIdiomas() as $idioma) {
                if($idioma->getIdioma()->getIdioma() == $oferta->getIdioma()->getIdioma()){
                    $pasa = true;
                    break;
                }         
            }    
            
            if(!$pasa){
                $curriculums->removeElement($cv);             
                $pasa = false;
//                $cvsFiltrados->add($cv);             
            }       
        }
        
//        return $cvsFiltrados;
    }
    
    private function filtrarPorTitulo($curriculums){
        $pasa = false;
        
        foreach ($curriculums as $cv) {
            $pasa = false;
            foreach ($cv->getEstudios() as $estudio) {
                if($estudio->getNivelEstudio() == "GRADO"){
                    $pasa = true;
                    break;
                }              
            }    
            
            if(!$pasa){
                $curriculums->removeElement($cv);             
                $pasa = false;
            }

        }
    }
    
    private function filtrarNoCumplenReq(ArrayCollection $curriculums){     
        foreach ($curriculums as $cv) {  
            if(!$cv->getCumpleReq()){
                $curriculums->removeElement($cv);             
            }
        }
    }

    private function puntuarEstudios($curriculums){
        foreach ($curriculums as $cv) {
            foreach ($cv->getEstudios() as $estudio) {
                if($estudio->getNivelEstudio() == "MAESTRIA" || $estudio->getNivelEstudio() == "DOCTORADO"){
                    $cv->addPuntaje(5);
                }elseif($estudio->getNivelEstudio() == "GRADO"){
                    $cv->addPuntaje(4);                    
                }else{
                    $cv->addPuntaje(0.5);
                }
            }
        }
    }
    
    
    private function puntuarExperienciaLaboral($curriculums){
        foreach ($curriculums as $cv) {
            foreach ($cv->getExperienciasLaborales() as $exp) {
                $fechaInicio = $exp->getFechaInicio();
                $fechaFin = $exp->getFechaFin();
                $diff = abs(strtotime($fechaFin->format('d-m-Y')) - strtotime($fechaInicio->format('d-m-Y')));
                $añosExp = round(($diff/(365*24*60*60)));
                
              
                $cv->addPuntaje($añosExp);
            }
        }
    }
    
    private function puntuarIdioma($curriculums, $ofertaLaboral){
        foreach ($curriculums as $cv) {
            foreach ($cv->getIdiomas() as $idioma) {
                if($ofertaLaboral->getIdioma() != null 
                    && $ofertaLaboral->getIdioma()->getId() == $idioma->getIdioma()->getId()){
                    
                    $cv->addPuntaje(1);
                    
                    if($idioma->getNivelOral() == 'ALTO' || $idioma->getNivelOral()== 'MEDIO'){
                        $cv->addPuntaje(0.5);
                    }
                    
                    if($idioma->getNivelLectura()== 'ALTO' || $idioma->getNivelLectura()== 'MEDIO'){
                        $cv->addPuntaje(0.5);
                    }
                }
            }
        }
    }
    
    private function puntuarRequerimientos(ArrayCollection $curriculums, $requerimientos, $filtrarNoCumplenRequisitos){
        $puntajeTotal = 0;
        $puntajeAcumulado = 0;
        
        foreach ($curriculums as $cv) {
            foreach ($requerimientos as $requerimiento) {
                $puntajeTotal += $requerimiento->getPuntaje();
                
                foreach ($cv->getConocimientos() as $conocimiento) {
                    if ($requerimiento->getTecnologia() == null) {
                        continue;
                    }
                    if($conocimiento->getTecnologia()->getId()
                       == $requerimiento->getTecnologia()->getId()){
                        
                        $cv->addPuntaje($requerimiento->getPuntaje());
                        
                        $puntajeAcumulado += $requerimiento->getPuntaje();
                    }
                }        
            }
            
            if($puntajeTotal == 0){
                $cv->setPorcentajeReqCumplidos(1);
            }else{
                if($filtrarNoCumplenRequisitos){
                    if($puntajeAcumulado/$puntajeTotal == 0){
                        $cv->setCumpleReq(false);
                    }else{                        
                        $cv->setPorcentajeReqCumplidos($puntajeAcumulado/$puntajeTotal);                                                    
                        $cv->setCumpleReq(true);
                    }
                }else{
                    $cv->setCumpleReq(true);
                    $cv->setPorcentajeReqCumplidos($puntajeAcumulado/$puntajeTotal);                                                                        
                }
            }
            
            $puntajeTotal = 0;
            $puntajeAcumulado = 0;
        }
    }
    
    public function showCurriculumPostulanteAction($idCurriculum){
        $curriculum = $this->getDoctrine()->getRepository('DefaultBundle:Curriculum')->find($idCurriculum);
        
        return $this->render('DefaultBundle::showCurriculum.html.twig', array('curriculum'=>$curriculum));
    }
}

?>
