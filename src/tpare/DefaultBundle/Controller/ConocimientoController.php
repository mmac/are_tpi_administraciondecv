<?php
namespace tpare\DefaultBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use tpare\DefaultBundle\Entity\Curriculum;
use tpare\DefaultBundle\Forms\ConocimientoType;
use tpare\DefaultBundle\Entity\Conocimiento;

/**
 * @author martinmac
 */
class ConocimientoController extends Controller{
    
    public function nuevoConocimientoAction(Request $request){
        
        $user=  $this->getUser();
        $curriculum = $user->getCurriculum();
        $conocimiento = new Conocimiento();
        
        /*
         * Lo busca en la BD, si no lo encuentra es NULL y el form va a estar vacio.
         */
        //$conocimiento= $this->getDoctrine()->getRepository('DefaultBundle:Conocimiento')->find($idConocimiento);
            
        $form = $this->createForm(new ConocimientoType(), $conocimiento);
        
        /*
         * Si el método es post, le hago un bind al formulario con los datos del request
         * y luego valido los datos. Si no entra al if, simplemente se renderiza el formulario vacio
         */
        if ($request->isMethod('POST')) {
            
            $form->bind($request);

            if ($form->isValid()) {
                
                $conocimiento = $form->getData();                

                $curriculum->addConocimiento($conocimiento);
                $conocimiento->setCurriculum($curriculum);
                
                //se tira el objeto a la BD.
                $em = $this->getDoctrine()->getManager();
                $em->persist($conocimiento);                 
                $em->persist($curriculum);
                $em->flush();
                
                 $this->get('session')->getFlashBag()->add(
                    'exito',
                    'Has registrado con éxito el conocimiento.'
                     );
                 
                return $this->redirect($this->generateUrl('default_show_curriculum', 
                        array('idCurriculum'=>$curriculum->getId())));
            }
            
            $this->get('session')->getFlashBag()->add(
                    'error',
                    'Hubo un error dentro del formulario. Por favor verifica los datos ingresados.'
                     );       
        }
     
        return $this->render('DefaultBundle:Formularios:nuevoConocimiento.html.twig', 
                 array('form'=>$form->createView(),));    
    }
    
    public function eliminarAction($idConocimiento){
        
        $user = $this->getUser(); 
        $curriculum = $user->getCurriculum();
         
        if($user == null){
            $this->get('session')->getFlashBag()->add(
                    'error',
                    'Por favor inicie sesion antes eliminar un conocimiento.'
                     );
            
            return $this->redirect($this->generateUrl('default_home'));
        }
        
        $conocimiento = $this->getDoctrine()->getRepository('DefaultBundle:Conocimiento')->find($idConocimiento);
        
        if($conocimiento != null){
            $em = $this->getDoctrine()->getManager();
            $em->remove($conocimiento);
            $em->flush();
                
            $this->get('session')->getFlashBag()->add(
                'exito',
                'El conocimiento se ha eliminado con éxito.'
            );
            
            return $this->redirect($this->generateUrl('default_show_curriculum', 
                   array('idCurriculum'=>$curriculum->getId())));
        }else{
            $this->get('session')->getFlashBag()->add(
                    'error',
                    'Ese conocimiento no existe.'
                     );
            
            return $this->redirect($this->generateUrl('default_home'));           
        }
        
        return $this->redirect($this->generateUrl('default_panel_empresa'));
        
     }
}

?>
