<?php
namespace tpare\DefaultBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use tpare\DefaultBundle\Entity\User;
use tpare\DefaultBundle\Entity\Curriculum;
use tpare\DefaultBundle\Forms\UserType;

/**
 * Description of UserController
 *
 * @author ale
 */
class UserController extends Controller{
    
    public function newUserAction(Request $request){
        $user = new User();
         
        $form = $this->createForm(new UserType(), $user);
       
        /*
         * Si el método es post, le hago un bind al formulario con los datos del request
         * y luego valido los datos. Si no entra al if, simplemente se renderiza el formulario vacio
         */
        if ($request->isMethod('POST')) {
            
            $form->bind($request);

            if ($form->isValid()) {
                
                $user = $form->getData();
                
                $curriculum = new Curriculum();
                $user->setCurriculum($curriculum);
                
                // 4 es el ROLE_USER, luego se asigna el ROLE_ADMIN manualmente desde la BD
                $role = $this->getDoctrine()->getRepository('DefaultBundle:Role')->find(3); 
                $user->addRole($role);
                
                //acá se codifica la contraseña para que no este en texto plano en la BD
                $factory = $this->get('security.encoder_factory');
                $encoder = $factory->getEncoder($user);
                $password = $encoder->encodePassword($user->getPassword(), $user->getSalt());
                $user->setPassword($password);

                //se tira el objeto a la BD.
                $em = $this->getDoctrine()->getManager();
                $em->persist($curriculum);
                $em->persist($user);
                $em->flush();
                
                 $this->get('session')->getFlashBag()->add(
                    'exito',
                    'Te has registrado con éxito. Ya puedes cargar tu curriculum y aplicar a los puestos existentes.'
                     );

                return $this->redirect($this->generateUrl('default_home'));
            }
            
            $this->get('session')->getFlashBag()->add(
                    'error',
                    'Hubo un error dentro del formulario. Por favor verifica los datos ingresados.'
                     );
         
        }
              
        return $this->render('DefaultBundle:Formularios:newUserForm.html.twig', array('form'=>$form->createView()));
     }
     
     public function updateAction(Request $request){
        $user = $this->getUser();
        
        if($user == null){
            $this->get('session')->getFlashBag()->add(
                    'error',
                    'Por favor inicie sesion antes de modificar los datos.'
                     );
            
            $this->redirect($this->generateUrl('default_home'));
        }
         
        $form = $this->createForm(new UserType(), $user);
       
        /*
         * Si el método es post, le hago un bind al formulario con los datos del request
         * y luego valido los datos. Si no entra al if, simplemente se renderiza el formulario vacio
         */
        if ($request->isMethod('POST')) {
            
            $form->bind($request);

            if ($form->isValid()) {
                
                $user = $form->getData();
                
                //acá se codifica la contraseña para que no este en texto plano en la BD
                $factory = $this->get('security.encoder_factory');
                $encoder = $factory->getEncoder($user);
                $password = $encoder->encodePassword($user->getUsername(), $user->getSalt());
                $user->setPassword($password);

                //se tira el objeto a la BD.
                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();
                
                 $this->get('session')->getFlashBag()->add(
                    'exito',
                    'Los datos han sido modificados con éxito.'
                     );

                return $this->redirect($this->generateUrl('default_panel_empresa'));
            }
            
            $this->get('session')->getFlashBag()->add(
                    'error',
                    'Hubo un error dentro del formulario. Por favor verifica los datos ingresados.'
                     );
         
        }
              
        return $this->render('DefaultBundle::panelEmpresa.html.twig', array('form'=>$form->createView()));
     }
}

?>
