<?php
namespace tpare\DefaultBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use tpare\DefaultBundle\Entity\Curriculum;
use tpare\DefaultBundle\Forms\EstudioType;
use tpare\DefaultBundle\Entity\Estudio;

class EstudioController extends Controller{
    
     public function nuevoEstudioAction(Request $request, $idEstudio){
        
        $user = $this->getUser();
        $curriculum = $user->getCurriculum();
        
        /*
         * Lo busca en la BD, si no lo encuentra es NULL y el form va a estar vacio.
         */
        $estudio = $this->getDoctrine()->getRepository('DefaultBundle:Estudio')->find($idEstudio);            

        
             
        $form = $this->createForm(new EstudioType(), $estudio);
        
        /*
         * Si el método es post, le hago un bind al formulario con los datos del request
         * y luego valido los datos. Si no entra al if, simplemente se renderiza el formulario vacio
         */
        if ($request->isMethod('POST')) {
            
            $form->bind($request);

            if ($form->isValid()) {
                
                $estudio = $form->getData();                
                
                $curriculum->addEstudio($estudio);
                $estudio->setCurriculum($curriculum);
                
                //se tira el objeto a la BD.
                $em = $this->getDoctrine()->getManager();
                $em->persist($estudio);                 
                $em->persist($curriculum);
                $em->flush();
                
                 $this->get('session')->getFlashBag()->add(
                    'exito',
                    'Has registrado con éxito el estudio.'
                     );
                 
                return $this->redirect($this->generateUrl('default_show_curriculum', 
                        array('idCurriculum'=>$curriculum->getId())));
            }
            
            $this->get('session')->getFlashBag()->add(
                    'error',
                    'Hubo un error dentro del formulario. Por favor verifica los datos ingresados.'
                     );       
        }
     
        return $this->render('DefaultBundle:Formularios:nuevoEstudio.html.twig', array('form'=>$form->createView(), 
            'idEstudio'=>$idEstudio));     
    }
    
    public function eliminarAction($idEstudio){
        $user = $this->getUser(); 
        $curriculum = $user->getCurriculum();
         
        if($user == null){
            $this->get('session')->getFlashBag()->add(
                    'error',
                    'Por favor inicie sesion antes eliminar un estudio.'
                     );
            
            return $this->redirect($this->generateUrl('default_home'));
        }
        
        $estudio = $this->getDoctrine()->getRepository('DefaultBundle:Estudio')->find($idEstudio);
        
        if($estudio != null){
            $em = $this->getDoctrine()->getManager();
            $em->remove($estudio);
            $em->flush();
                
            $this->get('session')->getFlashBag()->add(
                'exito',
                'El estudio se ha eliminado con éxito.'
            );
            
            return $this->redirect($this->generateUrl('default_show_curriculum', 
                   array('idCurriculum'=>$curriculum->getId())));
        }else{
            $this->get('session')->getFlashBag()->add(
                    'error',
                    'Ese estudio no existe.'
                     );
            
            return $this->redirect($this->generateUrl('default_home'));           
        }
        
        return $this->redirect($this->generateUrl('default_panel_empresa'));
        
     }
    
}

?>
