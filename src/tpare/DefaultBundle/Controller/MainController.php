<?php

namespace tpare\DefaultBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use tpare\DefaultBundle\Entity\Filter\OfertaFilter;
use tpare\DefaultBundle\Forms\Filter\OfertaFilterType;

class MainController extends Controller
{
    public function homeAction()
    {         
        $ofertasLaborales = $this->getDoctrine()->getRepository('DefaultBundle:OfertaLaboral')->buscarUltimasActivas();
        
        $ofertaFilter = new OfertaFilter();
        
        $form = $this->createForm(new OfertaFilterType(), $ofertaFilter);
        
        return $this->render('DefaultBundle::home.html.twig', array('ofertasLaborales'=>$ofertasLaborales, 'form'=>$form->createView()));
    }
}
