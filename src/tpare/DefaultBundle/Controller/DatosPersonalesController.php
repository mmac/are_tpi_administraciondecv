<?php
namespace tpare\DefaultBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use tpare\DefaultBundle\Entity\Curriculum;
use tpare\DefaultBundle\Forms\DatosPersonalesType;
use tpare\DefaultBundle\Entity\DatosPersonales;

/**
 * Description of DatosPersonalesController
 *
 * @author martinmac
 */
class DatosPersonalesController extends Controller{
    
     public function nuevosDatosPersonalesAction(Request $request){
        
        $user = $this->getUser();
        $curriculum = $user->getCurriculum();
        
        /*
         * Lo busca en la BD, si no lo encuentra es NULL y el form va a estar vacio.
         */
        $datosPersonales = $curriculum->getDatosPersonales();         
 
        
        $form = $this->createForm(new DatosPersonalesType(), $datosPersonales);
        
        /*
         * Si el método es post, le hago un bind al formulario con los datos del request
         * y luego valido los datos. Si no entra al if, simplemente se renderiza el formulario vacio
         */
        if ($request->isMethod('POST')) {
            
            $form->bind($request);

            if ($form->isValid()) {
                
                $datosPersonales = $form->getData();
                $curriculum->setDatosPersonales($datosPersonales);

                //se tira el objeto a la BD.
                $em = $this->getDoctrine()->getManager();
                $em->persist($datosPersonales);
                $em->persist($curriculum);
                $em->flush();
                
                $this->get('session')->getFlashBag()->add(
                    'exito',
                    'Has registrado con éxito los datos personales.'
                );
                 
                return $this->redirect($this->generateUrl('default_show_curriculum', 
                        array('idCurriculum'=>$curriculum->getId())));
            }
            
            $this->get('session')->getFlashBag()->add(
                    'error',
                    'Hubo un error dentro del formulario. Por favor verifica los datos ingresados.'
                     );      
        }
     
        return $this->render('DefaultBundle:Formularios:nuevosDatosPersonales.html.twig', array('form'=>$form->createView()));  
    }
}

?>
