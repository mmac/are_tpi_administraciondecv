<?php
namespace tpare\DefaultBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use tpare\DefaultBundle\Entity\Curriculum;
use tpare\DefaultBundle\Forms\ConocimientoType;
use tpare\DefaultBundle\Entity\Conocimiento;
use tpare\DefaultBundle\Entity\Aplicacion;


/**
 * Description of AplicacionController
 *
 * @author ale
 */
class AplicacionController extends Controller{
    
    public function nuevaAplicacionAction(Request $request, $idOferta){
        
        $user = $this->getUser();
        $oferta = $this->getDoctrine()->getRepository('DefaultBundle:OfertaLaboral')->find($idOferta);
        $aplicacionesUsuario = $user->getAplicaciones();
        
        foreach ($aplicacionesUsuario as $aplicacion) {
            if($aplicacion->getOfertaLaboral()->getId() == $idOferta){
                $this->get('session')->getFlashBag()->add(
                'error',
                'Ya has aplicado a esta oferta, no puedes hacerlo dos veces.'
                );
                
                return $this->redirect($this->generateUrl('default_home'));
            }
        }
        
        $aplicacion = new Aplicacion();
        $aplicacion->setOfertaLaboral($oferta);
        $aplicacion->setUser($user);
       
        $user->addAplicacione($aplicacion);
        
        $oferta->addAplicacione($aplicacion);
        
        $em = $this->getDoctrine()->getEntityManager();
        $em->persist($aplicacion);
        $em->persist($user);
        $em->persist($oferta);
        
        $this->get('session')->getFlashBag()->add(
                'exito',
                'Hás aplicado con éxito al puesto laboral.'
            );
        
        $em->flush();
        
        return $this->redirect($this->generateUrl('default_home'));
    }
}

?>
