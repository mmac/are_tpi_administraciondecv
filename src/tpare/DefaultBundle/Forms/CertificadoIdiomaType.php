<?php
namespace tpare\DefaultBundle\Forms;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use tpare\DefaultBundle\Entity\CertificadoIdioma;
use tpare\DefaultBundle\Entity\IdiomaPostulante;


class CertificadoIdiomaType extends AbstractType{
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('fechaEmision', 'date', array('label' => 'Fin', 'widget'=>'single_text',
                        'attr'=>array('class'=>'input-medium')))
                ->add('nombre', 'text', array('attr' => array('placeholder' => 'Nombre del certificado', 
                        'class'=>'input-xlarge')))
                ->add('institucion','text',array('label'=>'Institución',
                    'attr'=>array('class'=>'input-xlarge', 'placeholder'=>'Institucion')))
                
            ;
    }
    
    public function getName() {
        return 'certificadoIdiomaForm';
    }
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'tpare\DefaultBundle\Entity\CertificadoIdioma'));
    }
    
}

?>
