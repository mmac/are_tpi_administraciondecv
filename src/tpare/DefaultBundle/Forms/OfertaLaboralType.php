<?php

namespace tpare\DefaultBundle\Forms;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use tpare\DefaultBundle\Entity\OfertaLaboral;
use tpare\DefaultBundle\Forms\RequerimientoType;


/**
 * Description of OfertaLaboralType
 *
 * @author ale
 */
class OfertaLaboralType extends AbstractType{
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('titulo', 'text', array('attr' => array('placeholder' => 'Titulo de la oferta laboral', 
                        'class'=>'input-xlarge')))
                ->add('rol', 'entity', array(
                    'class' => 'DefaultBundle:Rol',
                    'property' => 'nombre',
                    'empty_value' => 'Selecciona un rol',
                    'attr'=>array('class'=>'input-medium')))
                ->add('jerarquia', 'choice', array('label' => 'Jerarquía', 'choices'=> OfertaLaboral::getJerarquiaTypes(),
                        'attr'=>array('class'=>'input-medium')))
                ->add('jornada', 'choice', array('label' => 'Jornada', 'choices'=> OfertaLaboral::getJornadaTypes(),))
                ->add('vacantes', 'integer', array('label' => 'Vacantes', 'attr' => array('placeholder' => 'Número de vacantes')))
                ->add('descripcion', 'textarea', array('attr' => array('placeholder' => 'Descripción del puesto de trabajo', 'class'=>'span4')))
                ->add('fechaInicio', 'date', array('label' => 'Inicio', 'widget'=>'single_text',
                        'attr'=>array('class'=>'input-medium')))
                ->add('fechaFin', 'date', array('label' => 'Fin', 'widget'=>'single_text',
                        'attr'=>array('class'=>'input-medium')))
                ->add('requerimientos', 'collection', array(
                            'type'=> new RequerimientoType(),
                            'allow_add'=> true,
                            'allow_delete' => true,
                            'by_reference' => false,
                            'prototype'=> true,
                            'prototype_name' => '__requerimiento__', ))
                ->add('localidad', 'entity', array(
                    'class' => 'DefaultBundle:Localidad',
                    'property' => 'nombre',
                    'empty_value' => 'Selecciona una localidad', 'attr' => array('id'=>'select-localidades')))
                ->add('idioma', 'entity', array(
                    'class' => 'DefaultBundle:Idioma',
                    'property' => 'idioma',
                    'empty_value' => 'Selecciona un idioma requerido', 'attr' => array()))     
                ->add('provincia', 'entity', array(
                    'class' => 'DefaultBundle:Provincia',
                    'property' => 'nombre',
                    'empty_value' => 'Selecciona una provincia', 'attr' => array('id'=>'select-provincias')));
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'tpare\DefaultBundle\Entity\OfertaLaboral'));
    }
    
    public function getName() {
        return 'nuevaOfertaLaboralForm';
    }    
}

?>
