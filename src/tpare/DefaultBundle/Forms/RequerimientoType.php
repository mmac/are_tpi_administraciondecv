<?php

namespace tpare\DefaultBundle\Forms;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use tpare\DefaultBundle\Entity\Requerimiento;

/**
 * Description of RequerimientoType
 *
 * @author ale
 */
class RequerimientoType extends AbstractType{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {        
        $builder->add('tecnologia', 'entity', array(
                    'class' => 'DefaultBundle:Tecnologia',
                    'property' => 'nombre',
                    'empty_value' => 'Selecciona una tecnologia',
                    'attr' => array('class'=>'input-large')))  
                ->add('nivel', 'choice', array('label' => 'Nivel', 'choices'=> Requerimiento::getNivelTypes(),
                        'attr'=>array('class'=>'input-small')))
                ->add('puntaje', 'integer', array('label' => 'Puntaje', 
                        'attr' => array('class'=>'input-small', 
                            'placeholder' => 'Puntaje',)));
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'tpare\DefaultBundle\Entity\Requerimiento'));
    }
    
    public function getName() {
        return 'requerimientoForm';
    }    
}

?>
