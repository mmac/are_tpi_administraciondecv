<?php
namespace tpare\DefaultBundle\Forms;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class InformacionExtraType extends AbstractType{
    public function buildForm(FormBuilderInterface $builder, array $options) {
         $builder->add('descripcion', 'text', array('attr' => array('placeholder' => 'Información extra', 
                        'class'=>'input-xlarge')))   
                 ;
    }
    public function getName() {
        return 'informacionExtraform'
        ;
    }
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
       $resolver->setDefaults(array(
            'data_class' => 'tpare\DefaultBundle\Entity\InformacionExtra'));
    }
}

?>
