<?php
namespace tpare\DefaultBundle\Forms;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use tpare\DefaultBundle\Entity\ExperienciaLaboral;

class ExperienciaLaboralType extends AbstractType{
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('empresa','text',array('label'=>'Empresa',
                    'attr'=>array('class'=>'input-xlarge', 'placeholder'=>'Empresa')))
                ->add('jerarquia', 'choice', array('label' => 'Jerarquía','empty_value' => 'Selecciona una jerarquia', 'choices'=> ExperienciaLaboral::getJerarquiaTypes(),
                        'attr'=>array('class'=>'input-medium')))
//                ->add('cargo','text',array('label'=>'Cargo',
//                    'attr'=>array('class'=>'input-xlarge', 'placeholder'=>'Cargo Ocupado')))
                ->add('cargo', 'entity', array(
                    'class' => 'DefaultBundle:Rol',
                    'property' => 'nombre',
                    'empty_value' => 'Seleccione un cargo', 'attr' => array('id'=>'select-cargo')))
                ->add('personasACargo', 'text', array('label' => 'Personas a cargo', 
                        'attr' => array('class'=>'input-medium', 
                            'placeholder' => 'Número')))
                ->add('fechaInicio', 'date', array('label' => 'Inicio', 'widget'=>'single_text',
                        'attr'=>array('class'=>'input-medium')))
                
                ->add('fechaFin', 'date', array('label' => 'Fin', 'widget'=>'single_text',
                        'attr'=>array('class'=>'input-medium')))
                 ->add('motivoFinalizacion', 'textarea', 
                        array('attr' => array('label' => 'Motivo Finalización','placeholder' => 'Motivo Finalizacion', 
                            'class'=>'span4')))
                ->add('descripcionFuncion', 'textarea', 
                        array('attr' => array('label' => 'Descripción de funcion','placeholder' => 'Funciones realizadas', 
                            'class'=>'span4')))
                
                ;
    }
    public function getName() {
        return 'nuevoExperienciaLaboralForm';
    }
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'tpare\DefaultBundle\Entity\ExperienciaLaboral',
            'cascade_validation'=>true));
    }
}

?>
