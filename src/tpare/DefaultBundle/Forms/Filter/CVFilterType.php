<?php
namespace tpare\DefaultBundle\Forms\Filter;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use tpare\DefaultBundle\Entity\OfertaLaboral;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Range;
use tpare\DefaultBundle\Forms\RequerimientoType;

/**
 * Description of CVFilterType
 *
 * @author ale
 */
class CVFilterType extends AbstractType{
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
                
          $builder
//                  ->add('rol', 'entity', array(
//                    'class' => 'DefaultBundle:Rol',
//                    'property' => 'nombre',
//                    'empty_value' => 'Selecciona un rol',
//                    'required' => true,
//                    'expanded' => true,
//                    'multiple' => true,
//                    'mapped'=>false,
//                    'attr' => array('class' => 'unstyled inline-checkboxAndRadio')))
                  ->add('tituloExcluyente', 'choice', array(
                    'choices' => array('si' => 'Si', 'no' => 'No'),
                    'expanded' => true, 'multiple' => false,))
                  ->add('idiomaExcluyente', 'choice', array(
                    'choices' => array('si' => 'Si', 'no' => 'No'),
                    'expanded' => true, 'multiple' => false, ))
                  ->add('listarCVReqNoCumplidos', 'choice', array(
                    'choices' => array('si' => 'Si', 'no' => 'No'),
                    'expanded' => true, 'multiple' => false, ))
                  ->add('sexo', 'choice', array(
                    'choices'   => array('MASCULINO' => 'Masculino', 'FEMENINO' => 'Femenino'),
                    'required' => true,
                    'expanded' => true,
                    'multiple' => true,))
                  ->add('cargo', 'entity', array(
                    'class' => 'DefaultBundle:Rol',
                    'property' => 'nombre',
                    'empty_value' => 'Seleccione un cargo', 'attr' => array('id'=>'select-cargo')))
                  ->add('anos', 'text', array('label' => 'Años', 
                      'attr' => array('placeholder' => 'Años de exp. mínima'),
                      ))
//                  ->add('jerarquia', 'choice', array(
//                    'choices'=> OfertaLaboral::getJerarquiaTypes(),
//                    'required' => true,
//                    'expanded' => true,
//                    'multiple' => true,
//                    'mapped'=>false,
//                    'attr' => array('class' => 'unstyled inline-checkboxAndRadio')))
                  ->add('tecnologia', 'entity', array(
                    'class' => 'DefaultBundle:Tecnologia',
                    'property' => 'nombre',
                    'empty_value' => 'Selecciona una tecnologia',
                    'required' => true,
                    'expanded' => true,
                    'multiple' => true,
                    'mapped' => false,
                    'attr' => array()))
                  ->add('requerimientos', 'collection', 
                          array('type' => new RequerimientoType(), 
                                'allow_add' => true,
                                'allow_delete' => true,
                                'by_reference' => false,
                                'cascade_validation' => true));
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
                  
            'data_class' => '\tpare\DefaultBundle\Entity\Filter\CVFilter',
        ));
    }
    
    public function getName() {
        return 'CVFilterForm';
    }    
}

?>
