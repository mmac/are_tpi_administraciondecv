<?php
namespace tpare\DefaultBundle\Forms\Filter;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use tpare\DefaultBundle\Forms\RequerimientoType;

/**
 * Filtros para el listado de currículums a la hora de evaluar las ofertas
 * laborales.
 *
 * @author pmarrone
 */
class ListadoCurriculumFilterType extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options)
    {        
        $builder->add('requerimientos', 'collection', array('type'=> new RequerimientoType(), 
                            'allow_add'=> true,
                            'allow_delete' => true,
                            'by_reference' => false,
                            'prototype'=> true,
                            'prototype_name' => '__requerimiento__', ));

    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'tpare\DefaultBundle\Entity\Filter\CurriculumFilter'));
    }
    
    public function getName() {
        return 'listadoCurriculumFilter';
    }  
}

?>
