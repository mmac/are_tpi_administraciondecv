<?php

namespace tpare\DefaultBundle\Forms\Filter;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use tpare\DefaultBundle\Entity\OfertaLaboral;

/**
 * Description of OfertaFilterType
 *
 * @author ale
 */
class OfertaFilterType extends AbstractType{
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
                
          $builder->add('rol', 'entity', array(
                    'class' => 'DefaultBundle:Rol',
                    'property' => 'nombre',
                    'empty_value' => 'Selecciona un rol',
                    'required' => true,
                    'expanded' => true,
                    'multiple' => true,
                    'mapped'=>false,
                    'attr' => array('class' => 'unstyled inline-checkboxAndRadio')))
                  ->add('jerarquia', 'choice', array(
                    'choices'=> OfertaLaboral::getJerarquiaTypes(),
                    'required' => true,
                    'expanded' => true,
                    'multiple' => true,
                    'mapped'=>false,
                    'attr' => array('class' => 'unstyled inline-checkboxAndRadio')))
                  ->add('tecnologia', 'entity', array(
                    'class' => 'DefaultBundle:Tecnologia',
                    'property' => 'nombre',
                    'empty_value' => 'Selecciona una tecnologia',
                    'required' => true,
                    'expanded' => true,
                    'multiple' => true,
                    'mapped' => false,
                    'attr' => array('class' => 'unstyled inline-checkboxAndRadio')));
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'tpare\DefaultBundle\Entity\Filter\OfertaFilter'));
    }
    
    public function getName() {
        return 'OfertaLaboralFilterForm';
    }    
}

?>
