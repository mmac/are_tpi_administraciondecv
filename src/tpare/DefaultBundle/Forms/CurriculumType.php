<?php

namespace tpare\DefaultBundle\Forms;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use tpare\DefaultBundle\Entity\Curriculum;
use tpare\DefaultBundle\Forms\EstudioType;
use tpare\DefaultBundle\Forms\DatosPersonalesType;
use tpare\DefaultBundle\Forms\IdiomaPostulanteType;
use tpare\DefaultBundle\Forms\ExperienciaLaboralType;
use tpare\DefaultBundle\Forms\ConocimientoType;
use tpare\DefaultBundle\Forms\InformacionExtraType;

class CurriculumType extends AbstractType{
    public function buildForm(FormBuilderInterface $builder, array $options) {
         $builder->add('estudios', 'collection', array(
                            'type'=> new EstudioType(),
                            'allow_add'=> true,
                            'by_reference' => false,
                            'prototype'=> true,))
         
                    ->add('datosPersonales', new DatosPersonalesType())
         
                    ->add('idiomas', 'collection', array(
                            'type'=> new IdiomaPostulanteType(),
                            'allow_add'=> true,
                            'by_reference' => false,
                            'prototype'=> true,))
                 
                    ->add('experienciasLaborales', 'collection', array(
                            'type'=> new ExperienciaLaboralType(),
                            'allow_add'=> true,
                            'by_reference' => false,
                            'prototype'=> true,))
                    ->add('conocimientos', 'collection', array(
                            'type'=> new ConocimientoType(),
                            'allow_add'=> true,
                            'allow_delete'=>true,
                            'by_reference' => false,
                            'prototype'=> true,))
                    ->add('informacionExtra', 'collection', array(
                            'type'=> new InformacionExtraType(),
                            'allow_add'=> true,
                            'allow_delete'=>true,
                            'by_reference' => false,
                            ));
    }
    
    public function getName() {
        return 'nuevoCurriculumForm';
    }
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
       $resolver->setDefaults(array(
            'data_class' => 'tpare\DefaultBundle\Entity\Curriculum', 
           'cascade_validation'=>true));
    }
}

?>
