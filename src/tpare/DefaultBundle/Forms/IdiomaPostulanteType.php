<?php

namespace tpare\DefaultBundle\Forms;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use tpare\DefaultBundle\Entity\IdiomaPostulante;
use tpare\DefaultBundle\Forms\CertificadoIdiomaType;

class IdiomaPostulanteType extends AbstractType{
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('idioma', 'entity', array(
                    'class' => 'DefaultBundle:Idioma',
                    'property' => 'idioma',
                    'empty_value' => 'Selecciona un idioma', 'attr' => array('id'=>'select-idiomas')))
                ->add('nivelLectura', 'choice', array('label' => 'Nivel de lectura', 'empty_value' => 'Selecciona un nivel',
                    'choices'=> IdiomaPostulante::getNiveles(),
                        'attr'=>array('class'=>'input-xlarge')))
                ->add('nivelOral', 'choice', array('label' => 'Nivel oral', 'empty_value' => 'Selecciona un nivel',
                    'choices'=> IdiomaPostulante::getNiveles(),
                        'attr'=>array('class'=>'input-xlarge')))
                ->add('nivelEscrito', 'choice', array('label' => 'Nivel de escritura', 'empty_value' => 'Selecciona un nivel',
                    'choices'=> IdiomaPostulante::getNiveles(),
                        'attr'=>array('class'=>'input-xlarge')))
                ->add('nivelComprension', 'choice', array('label' => 'Nivel de comprensión', 'empty_value' => 'Selecciona un nivel',
                    'choices'=> IdiomaPostulante::getNiveles(),
                        'attr'=>array('class'=>'input-xlarge')))
                 ->add('certificados', 'collection', array(
                            'type'=> new CertificadoIdiomaType(),
                            'allow_add'=> true,
                            'allow_delete' => true,
                            'by_reference' => false,
                            'prototype'=> true,
                            'prototype_name' => '__certificadoIdioma__', ))
                ;
    }
    public function getName() {
        return 'nuevoIdiomaPostulanteForm';
    }
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'tpare\DefaultBundle\Entity\IdiomaPostulante'));
    }
}

?>
