<?php
namespace tpare\DefaultBundle\Forms;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Description of CursoType
 *
 * @author ale
 */
class CursoType extends AbstractType{
    
     public function buildForm(FormBuilderInterface $builder, array $options) {
         
        $builder->add('titulo', 'text', array('attr' => array('placeholder' => 'Titulo del Curso', 
                        'class'=>'input-xlarge')))                                              
                ->add('descripcion', 'textarea', 
                        array('attr' => array('placeholder' => 'Descripción del Curso', 
                            'class'=>'span4')))                             
                ->add('institucion','text',array('label'=>'Institución',
                        'attr'=>array('class'=>'input-xlarge', 'placeholder'=>'Institucion')))
                ->add('fechaFin', 'date', array('label' => 'Fin', 'widget'=>'single_text',
                        'attr'=>array('class'=>'input-medium')))
                ->add('duracion', 'integer', array('label' => 'Duración en Horas', 
                        'attr' => array('class'=>'input-medium', 
                            'placeholder' => 'ej: 34')));
    }
    
    public function getName() {
        return 'nuevoCursoForm';
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'tpare\DefaultBundle\Entity\Curso'));
    }
}

?>
