<?php
namespace tpare\DefaultBundle\Forms;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use tpare\DefaultBundle\Entity\Estudio;


class EstudioType extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('titulo', 'text', array('attr' => array('placeholder' => 'Titulo del estudio', 
                        'class'=>'input-xlarge')))
                
                ->add('fechaInicio', 'date', array('label' => 'Inicio', 'widget'=>'single_text',
                        'attr'=>array('class'=>'input-medium')))
                
                ->add('fechaFin', 'date', array('label' => 'Fin', 'widget'=>'single_text',
                        'attr'=>array('class'=>'input-medium')))
                
                ->add('nivelEstudio', 'choice', array('label' => 'Nivel', 'empty_value' => 'Selecciona un nivel',
                    'choices'=> Estudio::getNivelTypes(),
                        'attr'=>array('class'=>'input-xlarge')))
                
                ->add('descripcion', 'textarea', 
                        array('attr' => array('placeholder' => 'Descripción del Estudio', 
                            'class'=>'span4')))
                
                ->add('estadoEstudio', 'choice', 
                        array('label' => 'Estado','empty_value' => 'Selecciona un estado', 'choices'=> Estudio::getEstadoTypes(),
                        'attr'=>array('class'=>'input-xlarge')))
                
                ->add('institucion','text',array('label'=>'Institución',
                    'attr'=>array('class'=>'input-xlarge', 'placeholder'=>'Institucion')))
                ->add('promedio', 'text', array('label' => 'Promedio', 
                        'attr' => array('class'=>'input-medium', 
                            'placeholder' => 'ej: 8.00')))
                ->add('materiasTotales', 'text', array('label' => 'Materias Totales', 
                        'attr' => array('class'=>'input-medium', 
                            'placeholder' => 'Número')))
                ->add('materiasAprobadas', 'text', array('label' => 'Materias Aprobadas', 
                        'attr' => array('class'=>'input-medium', 
                            'placeholder' => 'Número')))
                
                
                ;
    }
    public function getName() {
        return 'nuevoEstudioForm';
    }
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'tpare\DefaultBundle\Entity\Estudio',
            'cascade_validation'=>true));
    }
}

?>
