<?php

namespace tpare\DefaultBundle\Forms;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Description of UserType
 *
 * @author ale
 */
class UserType extends AbstractType {
    
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nombre', 'text', array('attr' => array('placeholder' => 'Nombre Completo')))
                ->add('apellido', 'text', array('attr' => array('placeholder' => 'Apellidos')))
                ->add('email', 'text', array('attr' => array('placeholder' => 'Dirección de Email')))
                ->add('username', 'text', array('label' => 'Usuario', 'attr' => array('placeholder' => 'Nombre de Usuario')))                
                ->add('password', 'repeated', array(
                    'type' => 'password',
                    'invalid_message' => 'Las contraseñas no concuerdan.',
                    'first_name'=>'pass',
                    'second_name'=>'confirm',
                    'first_options' => array('label' => 'Constraseña', 
                        'attr' => array('placeholder' => 'Contraseña')),
                    'second_options' => array('label' => 'Repita la contraseña', 
                        'attr' => array('placeholder' => 'Repita la Contraseña'))));
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'tpare\DefaultBundle\Entity\User'));
    }
    
    public function getName() {
        return 'nuevoUsuarioForm';
    }    
}

?>
