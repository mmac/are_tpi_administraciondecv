<?php
namespace tpare\DefaultBundle\Forms;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use tpare\DefaultBundle\Entity\DatosPersonales;


class DatosPersonalesType extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder ->add('fechaNacimiento', 'date', array('label' => 'Fecha de Nacimiento', 'widget'=>'single_text',
                        'attr'=>array('class'=>'input-xlarge')))
                ->add('hijos', 'text', array('label' => 'Hijos', 
                        'attr' => array('class'=>'input-medium', 
                            'placeholder' => 'Número')))  
                ->add('estadoCivil', 'choice', array('label' => 'Estado civil', 'empty_value' => 'Selecciona un estado civil',
                    'choices'=> DatosPersonales::getEstadosCiviles(),
                        'attr'=>array('class'=>'input-xlarge')))
                ->add('sexo', 'choice', array('label' => 'Sexo', 'empty_value' => 'Selecciona un sexo',
                    'choices'=> DatosPersonales::getSexoTypes(),
                        'attr'=>array('class'=>'input-xlarge')))
                ->add('nombre', 'text', array('label'=>'Nombre','attr' => array('placeholder' => 'Nombres', 
                        'class'=>'input-xlarge')))
                ->add('apellido','text',array('label'=>'Apellido',
                    'attr'=>array('class'=>'input-xlarge', 'placeholder'=>'Apellidos')))
                ->add('nacionalidad','text',array('label'=>'Nacionalidad',
                    'attr'=>array('class'=>'input-xlarge', 'placeholder'=>'Nacionalidad')))
                ->add('localidad', 'entity', array(
                    'class' => 'DefaultBundle:Localidad',
                    'property' => 'nombre',
                    'empty_value' => 'Selecciona una localidad', 'attr' => array('id'=>'select-localidades')))
                ->add('provincia', 'entity', array(
                    'class' => 'DefaultBundle:Provincia',
                    'property' => 'nombre',
                    'empty_value' => 'Selecciona una provincia', 'attr' => array('id'=>'select-provincias')))
                ->add('calle','text',array('label'=>'Calle',
                    'attr'=>array('class'=>'input-xlarge', 'placeholder'=>'Calle')))
                ->add('numeroCalle', 'text', array('label' => 'Numero Calle', 
                        'attr' => array('class'=>'input-medium', 
                            'placeholder' => 'Número')))  
                ->add('telefono1', 'text', array('label' => 'Telefono principal', 
                        'attr' => array('class'=>'input-medium', 
                            'placeholder' => 'Número')))  
                ->add('telefono2', 'text', array('label' => 'Telefono alternativo', 
                        'attr' => array('class'=>'input-medium', 
                            'placeholder' => 'Número')))  
                ->add('email', 'text', array('attr' => array('placeholder' => 'Dirección de Email')))                                              
                ;
    }
    public function getName() {
        return 'nuevoDatosPersonalesForm';
    }
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
       $resolver->setDefaults(array(
            'data_class' => 'tpare\DefaultBundle\Entity\DatosPersonales',
            'cascade_validation'=>true));
    }
}

?>
