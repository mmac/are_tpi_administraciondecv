<?php

namespace tpare\DefaultBundle\Forms;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use tpare\DefaultBundle\Entity\Conocimiento;

class ConocimientoType extends AbstractType{
    
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('tecnologia', 'entity', array(
                    'class' => 'DefaultBundle:Tecnologia',
                    'property' => 'nombre',
                    'empty_value' => 'Selecciona una tecnología', 'attr' => array('id'=>'select-tecnologias')))
                 ->add('nivelConocimiento', 'choice', array('label' => 'Nivel conocimiento', 'empty_value' => 'Selecciona un nivel',
                    'choices'=> Conocimiento::getNivelesType(),))
                 ->add('descripcion', 'textarea', 
                        array('attr' => array('placeholder' => 'Descripción del Conocimiento', 
                            'class'=>'span4')));
    }
    
    public function getName() {
        return 'nuevoConocimientoForm';
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'tpare\DefaultBundle\Entity\Conocimiento'));
    }
}

?>
